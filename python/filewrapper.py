"""Classes for wrapping an input H5 file to ease retrieving our objects"""

import abc
from typing import Any, Dict, Generator, List, Mapping, Optional, Tuple, Union
import copy
import pandas
import h5py

from pythanubis.histogram import Histogram

# TODO: file open modes


def _itergroup(group: h5py.Group) -> Dict[str, Union[Dict, Histogram]]:
    outdict = {}
    for k, v in group.items():
        if isinstance(v, h5py.Group):
            outdict[k] = _itergroup(v)
        else:
            outdict[k] = Histogram.read(v)
    return outdict


def _itersum(
    indict: Dict[str, Union[Dict, Histogram]],
    outdict: Dict[str, Union[Dict, Histogram]],
):
    for k, v in indict.items():
        try:
            out_v = outdict[k]
        except KeyError:
            outdict[k] = copy.deepcopy(v)
        else:
            if type(out_v) != type(v):
                raise TypeError(
                    f"Type mismatch for key {k} (old type is {type(out_v)}, new type is {type(v)})"
                )
            if isinstance(v, Mapping):
                _itersum(v, out_v)
            else:
                out_v += v


def nested_iter(
    dct: Mapping[str, Union[str, Any]]
) -> Generator[Tuple[List[str], Any], None, None]:
    """Iterate over a nested dictionary"""
    for k, v in dct.items():
        if isinstance(v, Mapping):
            for klist, v2 in nested_iter(v):
                yield [k] + klist, v2
        else:
            yield [k], v


class IFileWrapper(abc.ABC):
    """ABC describing the access paradigm"""

    def get_dataframe(self, path: str) -> pandas.DataFrame:
        """Retrieve a dataframe with the specified path"""
        pass

    def get_histogram(self, path: str) -> Histogram:
        """Retrieve a histogram with the specified path

        The path should be relative to the 'Histograms' directory
        """
        pass

    @abc.abstractproperty
    def histograms(self) -> Dict[str, Union[Dict, Histogram]]:
        """Get a nested dictionary containing all the histograms"""
        pass


class FileWrapper(IFileWrapper):
    """Class wrapping a single file"""

    def __init__(self, fname: str):
        self._fname = fname

    def get_dataframe(self, path: str) -> pandas.DataFrame:
        return pandas.read_hdf(self._fname, path)

    def get_histogram(self, path: str) -> Histogram:
        with h5py.File(self._fname, "r") as fp:
            try:
                return Histogram.read(fp[f"Histograms/{path}"])
            except KeyError:
                raise KeyError(path)

    @property
    def histograms(self) -> Dict[str, Union[Dict, Histogram]]:
        try:
            with h5py.File(self._fname, "r") as fp:
                return _itergroup(fp["Histograms"])
        except KeyError:
            return {}

    def write_dataframe(self, path: str, df: pandas.DataFrame):
        """Write a dataframe to the output file"""
        df.to_hdf(self._fname, path)

    def write_histogram(self, path: str, hist: Histogram):
        """Write a histogram to the output file"""
        with h5py.File(self._fname, "a") as fp:
            hist.write(fp, f"Histograms/{path}")


class MultiFileWrapper(IFileWrapper):
    """Class that wraps multiple input files: NB this is read-only"""

    def __init__(self, fnames: List[str]):
        self._fnames = fnames
        self._histograms = None

    def get_dataframe(self, path: str) -> pandas.DataFrame:
        return pandas.concat(pandas.read_hdf(fname, path) for fname in self._fnames)

    def get_histogram(self, path: str) -> Histogram:
        obj = self.histograms
        for name in path.split("/"):
            obj = obj[name]
        return obj

    @property
    def histograms(self) -> Dict[str, Union[Dict, Histogram]]:
        # Cache the return value
        if self._histograms is not None:
            return self._histograms

        self._histograms = {}
        for fname in self._fnames:
            _itersum(FileWrapper(fname).histograms, self._histograms)
        return self._histograms
