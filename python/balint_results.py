import numpy as np

from pythanubis.histogram import Histogram
from pythanubis.axes import CategoryAxis

n_events = 1e4

kaon_cutflow_dict = {
    "All": 42290,
    "$p_T$": 530.7,
    "cosTheta": 50.7,
    "0Jet": 42.2,
    "1Charged": 19.15,
    "0Charged": 8.21,
    "0Jet0Charged": 8.19,
}

neutron_cutflow_dict = {
    "All": 74060,
    "$p_T$": 102.757,
    "cosTheta": 10.79,
    "0Jet": 6.55,
    "1Charged": 2.273,
    "0Charged": 0.757,
    "0Jet0Charged": 0.743,
}

kaon_cutflow = Histogram(
    (CategoryAxis(" ", list(kaon_cutflow_dict)),),
    np.array(list(kaon_cutflow_dict.values())),
)

neutron_cutflow = Histogram(
    (CategoryAxis(" ", list(neutron_cutflow_dict)),),
    np.array(list(neutron_cutflow_dict.values())),
)

full_lumi_predictions = {
    "neutron": {
        "optimistic": {
            "0Jet": 181.5 * 1e5,
            "1Charged": 63.3 * 1e5,
            "0Charged": 19.9 * 1e5,
            "0Jet0Charged": 21.2 * 1e5,
        },
        "conservative": {
            "0Jet": 76.2 * 1e5,
            "1Charged": 23.5 * 1e5,
            "0Charged": 9.4 * 1e5,
            "0Jet0Charged": 9.0 * 1e5,
        },
    },
    "kaon": {
        "optimistic": {
            "0Jet": 666 * 1e5,
            "1Charged": 304.0 * 1e5,
            "0Charged": 131.3 * 1e5,
            "0Jet0Charged": 125.2 * 1e5,
        },
        "conservative": {
            "0Jet": 292.2 * 1e5,
            "1Charged": 130.0 * 1e5,
            "0Charged": 57.2 * 1e5,
            "0Jet0Charged": 58.5 * 1e5,
        },
    },
}
