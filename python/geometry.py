"""Contains functions for determining the interactions of particles with ANUBIS"""

from contextlib import contextmanager
import dataclasses
import math
import warnings

import numpy as np

# TODO: This all still uses the wrong convention with the y and x axes.
# This doesn't matter *now* because we mess up phi anyway, but if we change this in the future
# it will...


@dataclasses.dataclass(frozen=True)
class ANUBISParams:
    # The radius of the ATLAS cylinder [m]
    ATLAS_radius: float = 12.5
    # The length of the ATLAS cylinder [m]
    ATLAS_length: float = 44
    # The height at which the PX14 shaft opens [m]
    PX14_start_height: float = 24
    # The z value at which the PX14 shaft opens [m]
    PX14_start_z: float = 4.5
    # The radius of ANUBIS [m]
    ANUBIS_radius: float = 8.75
    # The maximum y value of ANUBIS (i.e. where the straight line cut-off starts for the pipes
    ANUBIS_max_y: float = 7.25
    # The heights of the 4 ANUBIS stations
    ANUBIS_station_1_height: float = 24
    ANUBIS_station_2_height: float = 42.5
    ANUBIS_station_3_height: float = 61.3
    ANUBIS_station_4_height: float = 80

    @property
    def ANUBIS_z_displacement(self):
        """The z coordinate of the center of ANUBIS"""
        return self.PX14_start_z + self.ANUBIS_radius

    @property
    def ANUBIS_pipe_opening_angle(self):
        """Opening angle of the straight line segment cut out of the detector disks to make space for the pipes"""
        return 2 * math.acos(self.ANUBIS_max_y / self.ANUBIS_radius)

    @property
    def ANUBIS_height(self):
        """The total height of ANUBIS"""
        return self.ANUBIS_station_heights[-1] - self.ANUBIS_station_heights[0]

    @property
    def ANUBIS_station_heights(self):
        """The heights of all stations"""
        return (
            self.ANUBIS_station_1_height,
            self.ANUBIS_station_2_height,
            self.ANUBIS_station_3_height,
            self.ANUBIS_station_4_height,
        )

    @property
    def min_theta(self):
        """The minimum theta value that intercepts ANUBIS"""
        return math.atan2(
            self.ANUBIS_station_heights[0],
            self.ANUBIS_z_displacement + self.ANUBIS_radius,
        )

    @property
    def max_theta(self):
        """The maximum theta value that intercepts ANUBIS"""
        return math.atan2(
            self.ANUBIS_station_heights[-1],
            self.ANUBIS_z_displacement - self.ANUBIS_radius,
        )

    @property
    def max_px14_entry_theta(self):
        """The maximum theta value that intercepts the opening of the PX14 shaft"""
        return math.atan2(
            self.ANUBIS_station_heights[0],
            self.ANUBIS_z_displacement - self.ANUBIS_radius,
        )

    @property
    def max_eta(self):
        """The maximum eta value that intercepts ANUBIS"""
        return -math.log(math.tan(self.min_theta / 2))

    @property
    def min_eta(self):
        """The minimum eta value that intercepts ANUBIS"""
        return -math.log(math.tan(self.max_theta / 2))

    @property
    def min_phi(self):
        """The minimum phi value that intercepts ANUBIS"""
        return math.atan2(-self.ANUBIS_radius, self.ANUBIS_station_heights[0])

    @property
    def max_phi(self):
        """The maximum phi value that intercepts ANUBIS"""
        return math.atan2(self.ANUBIS_max_y, self.ANUBIS_station_heights[0])


@contextmanager
def suppress_warnings(*messages):
    with warnings.catch_warnings():
        for message in messages:
            warnings.filterwarnings("ignore", message)
        yield


nan_warnings = ["All-NaN axis encountered", "All-NaN slice encountered"]
zero_divide_warning = "divide by zero encountered in true_divide"


class Geometry:
    """Provides helper functions to decide intersections

    Note that the functions here are designed to be used on columnar (array) values. They can be
    used with single (scalar) values but will then return scalar numpy arrays. In order to access
    the returned element you must index the returned array with an empty tuple.
    """

    def __init__(self, params: ANUBISParams = ANUBISParams()):
        self._params = params

    @property
    def parameters(self):
        """The detector geometry parameters"""
        return self._params

    def atlas_intersection(self, theta):
        """Get the distance along the particle trajectory that it leaves ATLAS"""
        return np.where(
            np.abs(np.tan(theta))
            < 2 * self.parameters.ATLAS_radius / self.parameters.ATLAS_length,
            # If True, angle is small (or large) enough to intersect with the endcaps
            self.parameters.ATLAS_length / (2 * np.cos(theta)),
            # Otherwise intercepts the barrel
            self.parameters.ATLAS_radius / np.sin(theta),
        )

    def _px14_circle_intersection(self, theta, phi):
        """Get the distances along the particle path that it intersects the circular cross-section of ANUBIS

        This will return a complex number if the path does not intersect the circle, i.e. it misses
        ANUBIS.
        """
        # this means solving the quadratic formula. The solution is
        # (z0*cos(theta) +- sqrt(r^2cos^2(theta) + (r^2 - z0^2)sin^2(theta)sin^(phi))/(cos^2(theta) + sin^2(theta)sin^2(phi)
        r = self.parameters.ANUBIS_radius
        z0 = self.parameters.ANUBIS_z_displacement
        ct = np.cos(theta)
        st = np.sin(theta)
        sp = np.sin(phi)
        # Precalculate the major pieces to speed things up
        denom = ct ** 2 + st ** 2 * sp ** 2
        a = z0 * ct / denom
        # use sqrt from emath which can return a complex number
        b = (
            np.emath.sqrt(r ** 2 * ct ** 2 + (r ** 2 - z0 ** 2) * st ** 2 * sp ** 2)
            / denom
        )
        return np.array([a - b, a + b])

    def _px14_line_intersection(self, theta, phi):
        """Return the distance along the particle trajectory that it intersects the line cut-off
        of ANUBIS
        """
        with suppress_warnings(zero_divide_warning):
            return self.parameters.ANUBIS_max_y / (np.sin(theta) * np.sin(phi))

    def px14_shaft_xsec_intersection(self, theta, phi):
        """Return the distances along the particle trajectory that it intersects the PX14 shaft"""
        # Get the intercepts with the circle of the shaft (in the yz plane)
        c = self._px14_circle_intersection(theta, phi)
        # Replace complex intercepts (misses) with nan
        circle = np.where(np.isreal(c), np.real(c), np.nan)
        # Replace any instances where the intersection is past the maximum y with the correct value
        return np.where(
            np.isfinite(circle)
            & (circle * np.sin(theta) * np.sin(phi) > self.parameters.ANUBIS_max_y),
            self._px14_line_intersection(theta, phi),
            circle,
        )

    def intercepts_anubis(self, theta, phi):
        """Return whether or not the particle trajectory intercepts ANUBIS at all"""
        slope = np.sin(theta) * np.cos(phi)
        entry_height, exit_height = self.px14_shaft_xsec_intersection(theta, phi) * slope
        return np.isfinite(entry_height) & (entry_height < self.parameters.ANUBIS_station_heights[-1]) & (self.parameters.ANUBIS_station_heights[0] < exit_height)


    def anubis_station_intercept(self, index, theta, phi):
        """Return the distance along the particle trajectory that it intercepts the specified ANUBIS
        station

        Note that this (unfortunately) uses a different convention to the naming in the parameters
        class, i.e. the first station here is indexed with 0.
        """
        # Get where the trajectory intercepts the shaft in the yz plane
        shaft_xsec_intercepts = self.px14_shaft_xsec_intersection(theta, phi)
        # Gives the slope of the trajectory - i.e. convert from distance of trajectory and the
        # height
        f = np.sin(theta) * np.cos(phi)
        entryexit_heights = f * shaft_xsec_intercepts
        height = self.parameters.ANUBIS_station_heights[index]
        return np.where(
            (entryexit_heights[0] < height) & (height < entryexit_heights[1]),
            height / f,
            np.nan,
        )

    def anubis_station_intercepts(self, theta, phi, shaft_xsec_intercepts=None):
        """Return the distance along the particle trajectory that it intercepts each ANUBIS
        station
        """
        # Get where the trajectory intercepts the shaft in the yz plane
        if shaft_xsec_intercepts is None:
            shaft_xsec_intercepts = self.px14_shaft_xsec_intersection(theta, phi)
        # Gives the slope of the trajectory - i.e. convert from distance of trajectory and the
        # height
        f = np.sin(theta) * np.cos(phi)
        entryexit_heights = f * shaft_xsec_intercepts
        return np.array(
            [
                np.where(
                    (entryexit_heights[0] < height) & (height < entryexit_heights[1]),
                    height / f,
                    np.nan,
                )
                for height in self.parameters.ANUBIS_station_heights
            ]
        )

    def anubis_wall_intercepts(self, theta, phi, shaft_xsec_intercepts=None):
        """Return the distances along the particle trajectory that it intercepts the PX14 walls"""
        # Get where the trajectory intercepts the shaft in the yz plane
        if shaft_xsec_intercepts is None:
            shaft_xsec_intercepts = self.px14_shaft_xsec_intersection(theta, phi)
        # Gives the slope of the trajectory - i.e. convert from distance of trajectory and the
        # height
        f = np.sin(theta) * np.cos(phi)
        entryexit_heights = f * shaft_xsec_intercepts
        return np.array(
            [
                np.where(
                    (entryexit_heights[0] > self.parameters.ANUBIS_station_heights[0])
                    & (
                        entryexit_heights[0]
                        < self.parameters.ANUBIS_station_heights[-1]
                    ),
                    shaft_xsec_intercepts[0],
                    np.nan,
                ),
                np.where(
                    (entryexit_heights[1] > self.parameters.ANUBIS_station_heights[0])
                    & (
                        entryexit_heights[1]
                        < self.parameters.ANUBIS_station_heights[-1]
                    ),
                    shaft_xsec_intercepts[1],
                    np.nan,
                ),
            ]
        )

    def all_anubis_intercepts(self, theta, phi):
        """Get all intercepts with ANUBIS (wall and station)"""
        shaft_xsec_intercepts = self.px14_shaft_xsec_intercept(theta, phi)
        station_intercepts = self.anubis_station_intercepts(
            theta, phi, shaft_xsec_intercepts
        )
        wall_intercepts = self.anubis_wall_intercepts(theta, phi, shaft_xsec_intercepts)
        return np.concatenate((station_intercepts, wall_intercepts))

    def _l1l2(self, all_intercepts):
        """Get the entry and exit for a full list of intercepts"""
        with suppress_warnings(*nan_warnings):
            return np.array(
                [np.nanmin(all_intercepts, axis=0), np.nanmax(all_intercepts, axis=0)]
            )

    def _entryexit(self, entry, exit):
        """Return the entry and exit

        Checks if the provided values are the same or either is nan
        """
        return np.where(
            (entry == exit) | np.isnan(entry) | np.isnan(exit),
            np.nan,
            (entry, exit),
        )

    def l1l2Conservative(self, theta, phi):
        """Old conservative geometry: bugged

        Bugged because it allows entry through a wall, not a station
        """
        with suppress_warnings(*nan_warnings):
            return self._entryexit(
                np.nanmin(self.all_anubis_intercepts(theta, phi), axis=0),
                np.nanmax(self.anubis_station_intercepts(theta, phi), axis=0),
            )

    def l1l2FixedConservative(self, theta, phi):
        """L1L2 for the fixed conservative geometry"""
        return self._l1l2(self.anubis_station_intercepts(theta, phi))

    def l1l2Optimistic(self, theta, phi):
        """L1L2 for the optimistic geometry"""
        with suppress_warnings(*nan_warnings):
            return self._entryexit(
                self.atlas_intersection(theta),
                np.nanmax(self.anubis_station_intercepts(theta, phi), axis=0),
            )

    def l1l2BuggedOptimistic(self, theta, phi):
        """L1L2 for the old, bugged optimistic geometry

        Bugged because it considers exit to be through the first ANUBIS station
        """
        return self._entryexit(
            self.atlas_intersection(theta, phi),
            self.anubis_station_intercept(0, theta, phi),
        )

    def is_in_theta_acceptance(self, theta):
        """Check if the trajectory is in theta acceptance"""
        return (self.parameters.min_theta < theta) & (theta < self.parameters.max_theta)

    def rescale_phi(self, phi):
        """Recale the phi coordinate to lie inside ANUBIS"""
        return self.parameters.min_phi + (
            self.parameters.max_phi - self.parameters.min_phi
        ) * np.remainder(phi, 2 * np.pi) / (2 * np.pi)

    def mirror_theta(self, theta):
        """Mirror the theta coordinate"""
        return np.where(theta > np.pi / 2, np.pi - theta, theta)

    def rescaled_phi_weight(self):
        """Provide a weight for the phi rescaling"""
        return (self.parameters.max_phi - self.parameters.min_phi) / (2 * np.pi)

    def l1l2_cavern_plus_shaft(self, theta, phi):
        """Return the exit and entry lengths for the given trajectory"""
        shaft_xsec_intercepts = self.px14_shaft_xsec_intersection(theta, phi)

        with suppress_warnings(*nan_warnings):
            exit = np.nanmax(
                self.anubis_station_intercepts(theta, phi, shaft_xsec_intercepts),
                axis=0,
            )
            # Distance along the trajectory that the trajectory intercepts the shaft wall
            wall_entry = np.nanmin(
                self.anubis_wall_intercepts(theta, phi, shaft_xsec_intercepts), axis=0
            )
            return np.where(
                # If theta is less than the critical value
                theta < self.parameters.max_px14_entry_theta,
                # The active volume begins at the edge of ATLAS
                self._entryexit(self.atlas_intersection(theta), exit),
                # Otherwise the active volume begins at the ANUBIS wall
                self._entryexit(wall_entry, exit),
            )

    def l1l2_shaft_only(self, theta, phi):
        """Return the entry and exit lengths for the given trajectory"""
        return self._l1l2(self.anubis_station_intercepts(theta, phi))

    def l1l2_concrete(self, theta, phi):
        """Return the entry and exit points for the concrete for the given trajectory"""
        with suppress_warnings(
            *nan_warnings, zero_divide_warning
        ):  # Distance along the trajectory that it intersects the shaft wall
            shaft_entry = np.nanmin(self.anubis_wall_intercepts(theta, phi), axis=0)
            ceiling_entry = self.parameters.PX14_start_height / (
                np.sin(theta) * np.cos(phi)
            )
            shape = theta.shape if isinstance(theta, np.ndarray) else ()
            return np.where(
                (theta < self.parameters.max_px14_entry_theta),
                (np.full(shape, 0), np.full(shape, 0)),
                (ceiling_entry, shaft_entry),
            )
