from pythanubis.indexhelpers import BinIndexer
from typing import List, Tuple, Union
from numbers import Real
import numpy as np
import matplotlib.pyplot as plt
import bisect
import math
from abc import ABC, abstractproperty, abstractmethod
from .composite import read_composite


class Axis(ABC):
    @abstractproperty
    def edges(self) -> List[float]:
        """The bin edges"""
        pass

    @abstractproperty
    def label(self) -> str:
        """The axis label"""
        pass

    @abstractproperty
    def n_bins(self) -> int:
        """The number of bins"""
        pass

    @property
    def full_n_bins(self) -> int:
        """The full number of bins including under/overflow"""
        # Rely on the fact that a bool behaves as an int with a value of either 1 (True) or 0 (False)
        return self.n_bins + sum(self.underoverflow)

    @abstractproperty
    def underoverflow(self) -> Tuple[bool, bool]:
        """Whether there are under and overflow bins

        The first element is true if there is an underflow bin, the second is true if there is
        an overflow bin
        """
        pass

    def get_slice(
        self, include_underflow: bool = True, include_overflow: bool = True
    ) -> slice:
        """Get a python slice for the counts/sumW2 array"""
        under, over = self.underoverflow
        base = int(under)
        start = 0 if include_underflow else base
        end = base + self.n_bins + (1 if over and include_overflow else 0)
        return slice(start, end)

    def format_axis(self, axis: plt.Axes, axis_name: str) -> None:
        """Format a matplotlib axes object"""
        getattr(axis, f"set_{axis_name}label")(self.label)

    @abstractmethod
    def find_bin(self, value: Union[str, Real]) -> int:
        """Find the bin index that corresponds to the provided value"""
        pass

    @abstractmethod
    def reduce_array(
        self,
        array: np.ndarray,
        selection: Union[slice, int, str, Tuple[str], BinIndexer],
        axis: int,
    ):
        """Reduce an array along the given axis making the corresponding selection"""
        pass

    @abstractmethod
    def __getitem__(self, selection: Union[slice, Tuple[str], BinIndexer]):
        """Get a new axis corresponding to the provided slice"""
        pass

    @classmethod
    def read(cls, data):
        if not isinstance(data, dict):
            data = read_composite(data)
        keys = set(data)
        if keys == {"name", "data"}:
            return FixedBinAxis.read(data)
        elif keys == {"name", "edges"}:
            return VariableBinAxis.read(data)
        elif keys == {"name", "categories"}:
            return CategoryAxis.read(data)
        else:
            raise ValueError(f"Cannot interpret '{data}' as an axis")

    @abstractproperty
    def numpy_dtype(self) -> np.dtype:
        """Get the numpy datatype of this axis"""
        pass

    @abstractmethod
    def to_numpy(self) -> np.ndarray:
        """Make this a numpy object (for H5 serialisation)"""
        pass


class NumericAxis(Axis):
    @property
    def underoverflow(self) -> Tuple[bool, bool]:
        return [True, True]

    def reduce_array(
        self, array: np.ndarray, selection: Union[slice, int, BinIndexer], axis: int
    ):
        if isinstance(selection, BinIndexer):
            selection = selection(self)
        if isinstance(selection, slice):
            start, stop, step = selection.indices(self.full_n_bins)
            start = max(1, start)
            stop = min(self.full_n_bins - 1, stop)
            indices = list(range(self.full_n_bins))[slice(start, stop, step)]
            # We always want to have the full underflow so make sure the indices start from 0
            indices = [0] + indices + [indices[-1] + step]
            return np.add.reduceat(array, indices, axis=axis)
        else:
            return np.take(array, indices=selection, axis=axis)

    def __eq__(self, other):
        if isinstance(other, NumericAxis):
            return self.label == other.label and self.edges == other.edges
        raise NotImplementedError


class FixedBinAxis(NumericAxis):
    """Axis type with fixed width bins"""

    def __init__(self, label: str, n_bins: int, min: float, max: float):
        """Create a fixed bin axis"""
        self._label = label
        self._n_bins = int(n_bins)
        self._min = min
        self._max = max

    @property
    def label(self) -> str:
        """The axis label"""
        return self._label

    @property
    def n_bins(self) -> int:
        """The number of bins"""
        return self._n_bins

    @property
    def min(self) -> float:
        """The minimum value"""
        return self._min

    @property
    def max(self) -> float:
        """The maximum value"""
        return self._max

    @property
    def edges(self) -> List[float]:
        """The bin edges"""
        return list(np.linspace(self.min, self.max, self.n_bins + 1))

    def __getitem__(self, x: Union[slice, BinIndexer]):
        """Get a new axis corresponding to the provided slice"""
        if callable(x):
            x = x(self)
        edges = self.edges
        start, stop, step = x.indices(len(edges))
        # Indexing runs from 1 to n_bins with 0 and n_bins+1 being under and overflow
        # Given that we always include these then 1:-1 is equivalent to 0:N We also want to include
        # the lower edge of bin one so reduce start by 1
        start = max(1, start) - 1
        stop = min(self.n_bins + 1, stop) - 1
        n = math.ceil((stop - start) / step)
        true_stop = start + step * n
        return FixedBinAxis(self.label, n, edges[start], edges[true_stop])

    def reduce_array(self, array, x, axis):
        if isinstance(x, BinIndexer):
            x = x(self)
        if isinstance(x, slice):
            start, stop, step = x.indices(self.full_n_bins)
            # Indexing runs from 1 to n_bins with 0 and n_bins+1 being under and overflow
            # Given that we always include these then 1:-1 is equivalent to 0:N
            start = max(1, start)
            stop = min(self.full_n_bins - 1, stop)
            indices = list(range(self.full_n_bins))[slice(start, stop, step)]
            # We always want to have the full underflow so make sure the indices start from 0
            indices = [0] + indices + [indices[-1] + step]
            return np.add.reduceat(array, indices, axis=axis)
        else:
            return np.take(array, indices=x, axis=axis)

    def find_bin(self, value):
        if value < self.min:
            return 0
        elif value >= self.max:
            return self.n_bins + 1
        else:
            width = (self.max - self.min) / self.n_bins
            return int((value - self.min) / width) + 1

    @classmethod
    def read(cls, data):
        if not isinstance(data, dict):
            data = read_composite(data)
        return cls(
            label=data["name"].decode("utf-8"),
            n_bins=data["data"]["nBins"],
            min=data["data"]["min"],
            max=data["data"]["max"],
        )

    @property
    def numpy_dtype(self) -> np.dtype:
        return np.dtype(
            [
                ("name", f"S{len(self.label)}"),
                (
                    "data",
                    [
                        ("nBins", np.uint64),
                        ("min", np.float64),
                        ("max", np.float64),
                    ],
                ),
            ]
        )

    def to_numpy(self) -> np.ndarray:
        return np.asarray(
            (self.label.encode("utf-8"), (self.n_bins, self.min, self.max)),
            dtype=self.numpy_dtype,
        )


class VariableBinAxis(NumericAxis):
    """Axis type with variable bin widths"""

    def __init__(self, label: str, edges: List[float]):
        """Create a variable bin axis"""
        self._label = label
        self._edges = edges

    @property
    def label(self) -> str:
        """The axis label"""
        return self._label

    @property
    def edges(self) -> List[float]:
        """The bin edges"""
        return self._edges

    @property
    def n_bins(self) -> int:
        """The number of bins"""
        return len(self.edges) - 1

    def __getitem__(self, x: Union[slice, BinIndexer]):
        """Get a new axis corresponding to the provided slice"""
        if callable(x):
            x = x(self)
        edges = self.edges
        start, stop, step = x.indices(len(edges))
        # Indexing runs from 1 to n_bins with 0 and n_bins+1 being under and overflow
        # Given that we always include these then 1:-1 is equivalent to 0:N. We also want to include
        # the lower edge of bin one so reduce start by 1
        start = max(1, start) - 1
        stop = min(self.n_bins + 1, stop)
        return VariableBinAxis(self.label, self.edges[slice(start, stop, step)])

    def find_bin(self, value):
        return bisect.bisect(self.edges, value)

    @classmethod
    def read(cls, data):
        if not isinstance(data, dict):
            data = read_composite(data)
        return cls(label=data["name"].decode("utf-8"), edges=data["edges"])

    @property
    def numpy_dtype(self) -> np.dtype:
        return np.dtype(
            [
                ("name", f"S{len(self.label)}"),
                ("edges", np.float64, len(self.edges)),
            ]
        )

    def to_numpy(self) -> np.ndarray:
        return np.asarray(
            (self.label.encode("utf-8"), self.edges), dtype=self.numpy_dtype
        )


class CategoryAxis(Axis):
    """Axis with discrete named categories"""

    def __init__(self, label: str, categories: List[str]):
        """Create a category axis"""
        self._label = label
        self._categories = categories

    @property
    def label(self) -> str:
        """The axis label"""
        return self._label

    @property
    def categories(self) -> str:
        """The axis categories"""
        return self._categories

    @property
    def n_bins(self) -> int:
        """The number of bins"""
        return len(self.categories)

    @property
    def underoverflow(self) -> Tuple[bool, bool]:
        return [False, True]

    @property
    def edges(self):
        """A fake list of bin edges used for building histograms"""
        return list(range(self.n_bins + 1))

    def format_axis(self, axis: plt.Axes, axis_name: str):
        super().format_axis(axis, axis_name)
        getattr(axis, f"set_{axis_name}ticks")([x + 0.5 for x in range(self.n_bins)])
        getattr(axis, f"set_{axis_name}ticklabels")(
            self.categories, rotation=-45, rotation_mode="anchor", ha="left"
        )

    def __getitem__(self, x: Union[Tuple[str], BinIndexer]):
        if callable(x):
            return CategoryAxis(self.label, self.categories[x(self)])
        else:
            return CategoryAxis(self.label, list(x))

    def reduce_array(self, array, x, axis):
        if isinstance(x, BinIndexer):
            x = x(self)
            if isinstance(x, slice):
                x = self.categories[x]
        if isinstance(x, (tuple, list)):
            # All indices not used in the reduction are overflow
            overflow = set(range(self.full_n_bins))
            indices = tuple(map(self.find_bin, x))
            new_array = np.take(array, indices=indices, axis=axis)
            overflow -= set(indices)
            overflow_array = np.take(array, indices=list(overflow), axis=axis)
            return np.append(
                new_array, np.sum(overflow_array, axis=axis, keepdims=True), axis=axis
            )
        else:
            if isinstance(x, int):
                idx = x
            else:
                idx = self.find_bin(x)
            return np.take(array, indices=idx, axis=axis)

    def find_bin(self, value):
        try:
            return self.categories.index(value)
        except ValueError:
            return self.n_bins

    @classmethod
    def read(cls, data):
        if not isinstance(data, dict):
            data = read_composite(data)
        return cls(
            label=data["name"].decode("utf-8"),
            categories=[b.decode("utf-8") for b in data["categories"].values()],
        )

    @property
    def numpy_dtype(self) -> np.dtype:
        return np.dtype(
            [
                ("name", f"S{len(self.label)}"),
                (
                    "categories",
                    [
                        (f"element{i}", f"S{len(v)}")
                        for i, v in enumerate(self.categories)
                    ],
                ),
            ]
        )

    def to_numpy(self) -> np.ndarray:
        return np.asarray(
            (
                self.label.encode("utf-8"),
                tuple(np.asarray(c, dtype=f"S{len(c)}") for c in self.categories),
            ),
            dtype=self.numpy_dtype,
        )

    def __eq__(self, other):
        if isinstance(other, CategoryAxis):
            return self.label == other.label and self.categories == other.categories
        raise NotImplementedError
