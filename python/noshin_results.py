"""Simple script just turning Noshin's results (in her paper) to H5 cutflows"""

import numpy as np

from pythanubis.histogram import Histogram
from pythanubis.axes import CategoryAxis

n_events = 1e10

# These numbers come from the spreadsheets and have MET > 20 GeV

kaon_cutflow_dict = {
    "All": 43728192752,
    "$p_T$": 618392749,
    "cosTheta": 48283723,
    "met": 16291,
    "metDPhi": 13124,
    "0Jet": 502,
    "1Charged": 775,
    "0Charged": 832,
    "0Jet0Charged": 489,
}

neutron_cutflow_dict = {
    "All": 73427192756,
    "$p_T$": 96192632,
    "cosTheta": 12981636,
    "met": 14196,
    "metDPhi": 12781,
    "0Jet": 361,
    "1Charged": 508,
    "0Charged": 522,
    "0Jet0Charged": 287,
}

kaon_cutflow = Histogram(
    (CategoryAxis(" ", list(kaon_cutflow_dict)),),
    np.array(list(kaon_cutflow_dict.values())),
)

neutron_cutflow = Histogram(
    (CategoryAxis(" ", list(neutron_cutflow_dict)),),
    np.array(list(neutron_cutflow_dict.values())),
)

# The summed (kaon + neutron) cutflow for 30 GeV
n_events_30GeV = 1e9
summed_cutflow_30GeV_dict = {
    "All": 11905300000,
    "$p_T$": 123625930,
    "cosTheta": 10901066,
    "met": 290,
    "metDPhi": 240,
    "0Jet": 9,
    "0Charged": 11,
    "1Charged": 10,
    "0Jet0Charged": 8,
}
summed_cutflow_30GeV = Histogram(
    (CategoryAxis(" ", list(summed_cutflow_30GeV_dict)),),
    np.array(list(summed_cutflow_30GeV_dict.values())),
)

# Actual number of background events predicted in the optimistic case
# Note that this selection has MET > 30 GeV
full_lumi_predictions = {
    "0Jet": 6.61,
    "1Charged": 6.63,
    "0Charged": 6.63,
    "0Jet0Charged": 6.59,
}
