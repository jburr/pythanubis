"""Script to export a CSV to be read by Mathematica"""

import argparse
import pandas
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", help="The input H5 file")
parser.add_argument("-o", "--output", default="candidates.csv", help="The output CSV")
parser.add_argument("-d", "--dataframe", help="The names of the dataframe to convert")
parser.add_argument("-m", "--mass", help="The mass of the particle")

args = parser.parse_args()
if args.mass is None:
    if args.dataframe == "k0Ls":
        mass = 0.497611
    elif args.dataframe == "neutrons":
        mass = 0.939565
    else:
        raise ValueError(f"No default mass for {args.dataframe}")
else:
    mass = args.mass

df = pandas.read_hdf(args.input, args.dataframe)
df["mass"] = mass
df["px"] = df["pt"] * np.cos(df["phi"])
df["py"] = df["pt"] * np.sin(df["phi"])
df["pz"] = df["pt"] * np.sinh(df["eta"])
df["p"] = df["pt"] * np.cosh(df["eta"])
df["energy"] = np.sqrt(df["p"] * df["p"] + mass * mass)
with open(args.output, "w") as fp:
    df.to_csv(fp, columns=["px", "py", "pz", "energy", "mass"], index_label="index")
