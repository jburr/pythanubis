from typing import Callable, List, Union, Tuple, Iterable
import itertools

import h5py


from .axes import Axis, NumericAxis, CategoryAxis
from .indexhelpers import BinIndexer, full_range
from .composite import read_composite
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm
import copy


class Histogram:
    def __init__(
        self, axes: Tuple[Axis], counts: np.ndarray = None, sum_w2: np.ndarray = None
    ):
        self._axes = tuple(map(copy.deepcopy, axes))
        full_shape = self.full_shape
        if counts is None:
            self._counts = np.zeros(full_shape)
        elif counts.size == np.prod(self.core_shape):
            # Handle the case where the user has supplied no overflow bins
            # Pad with zeroes where those bins should have been
            self._counts = np.pad(
                counts.reshape(self.core_shape),
                [list(map(int, a.underoverflow)) for a in self.axes],
            )
        else:
            self._counts = counts.reshape(full_shape)
        if sum_w2 is None:
            # If counts were provided and sum w2 was not, assume that all weights were 1
            # i.e. that the sumw2 should be the same as the counts
            self._sumw2 = self._counts.copy()
        elif sum_w2.size == np.prod(self.core_shape):
            # Handle the case where the user has supplied no overflow bins
            # Pad with zeroes where those bins should have been
            self._sumw2 = np.pad(
                sum_w2.reshape(self.core_shape),
                [list(map(int, a.underoverflow)) for a in self.axes],
            )
        else:
            self._sumw2 = sum_w2.reshape(full_shape)

    @classmethod
    def bin_data(cls, axes: List[NumericAxis], series, weights=None):
        if len(axes) != len(series):
            raise ValueError("Number of axes must match the number of series")
        edge_lists = [
            np.concatenate(([-np.inf], axis.edges, [np.inf])) for axis in axes
        ]
        series = np.stack(series, axis=-1)
        if weights is not None and not isinstance(weights, np.ndarray):
            weights = np.full(series.shape[0], weights)
        counts, _ = np.histogramdd(series, edge_lists, weights=weights)
        if weights is not None:
            sumw2, _ = np.histogramdd(series, edge_lists, weights=weights * weights)
        else:
            sumw2 = None
        return cls(axes, counts, sumw2)

    @property
    def n_dims(self):
        """The number of dimensions in the histogram"""
        return len(self.axes)

    @property
    def counts(self):
        return self._counts

    @property
    def sum_w2(self):
        return self._sumw2

    @property
    def full_shape(self):
        return tuple(a.full_n_bins for a in self.axes)

    @property
    def core_shape(self):
        return tuple(a.n_bins for a in self.axes)

    @property
    def axes(self) -> Tuple[Axis]:
        """The axes"""
        return tuple(self._axes)

    def integral(self, ranges=None):
        if ranges is None:
            ranges = itertools.repeat(full_range)
        slices = tuple(r(a) for a, r in zip(self.axes, ranges))
        return np.sum(self.counts[slices]), np.sum(self.sum_w2[slices])

    def project(self, axis, ranges=None):
        if isinstance(axis, int):
            axis = (axis,)
        if ranges is None:
            ranges = itertools.repeat(full_range)
        slices = []
        new_axes = []
        for idx, (a, r) in enumerate(zip(self.axes, ranges)):
            if idx in axis:
                slices.append(r(a))
            else:
                slices.append(r(a))
                new_axes.append(a[r])
        slices = tuple(slices)
        counts = np.sum(self.counts[slices], axis=axis)
        sumw2 = np.sum(self.sum_w2[slices], axis=axis)
        return Histogram(new_axes, counts, sumw2)

    def find_bin(self, *values):
        return tuple(a.find_bin(v) for a, v in zip(self.axes, values))

    def __getitem__(
        self,
        x: Union[int, str, BinIndexer, Tuple[Union[int, str, BinIndexer, Tuple[str]]]],
    ):
        if self.n_dims == 1 and (not isinstance(x, tuple) or len(x) != 1):
            x = (x,)

        # Have to reduce the counts properly
        counts = self.counts
        sum_w2 = self.sum_w2
        # Deal with cases where reducing along an axis can remove a whole axis
        axis_offset = 0
        new_axes = []
        for idx, (axis, selection) in enumerate(zip(self.axes, x)):
            new_counts = axis.reduce_array(counts, selection, idx - axis_offset)
            sum_w2 = axis.reduce_array(sum_w2, selection, idx - axis_offset)
            if new_counts.ndim != counts.ndim:
                axis_offset += 1
            else:
                new_axes.append(axis[selection])
            counts = new_counts

        if isinstance(counts, np.ndarray):
            return Histogram(new_axes, counts, sum_w2)
        else:
            return counts, sum_w2

    def __setitem__(self, indices, value):
        if not isinstance(indices, tuple):
            indices = (indices,)
        new_indices = []
        for axis, index in zip(self.axes, indices):
            if isinstance(axis, CategoryAxis) and isinstance(index, str):
                new_indices.append(axis.find_bin(index))
            else:
                new_indices.append(index)
        indices = tuple(new_indices)

        if isinstance(value, tuple):
            count, sumw2 = value
        else:
            count = value.counts
            sumw2 = value.sum_w2
        self._counts[indices] = count
        self._sumw2[indices] = sumw2

    def __mul__(self, scale):
        return Histogram(
            copy.deepcopy(self.axes), self._counts * scale, self._sumw2 * scale * scale
        )

    def __truediv__(self, value):
        return self * (1 / value)

    def normalise(self) -> "Histogram":
        return self / self.integral()[0]

    def __iadd__(self, other: "Histogram"):
        if self.axes != other.axes:
            raise ValueError("Cannot add histograms with different axes")
        self._counts += other.counts
        self._sumw2 += other.sum_w2
        return self

    def __add__(self, other: "Histogram"):
        if self.axes != other.axes:
            raise ValueError("Cannot add histograms with different axes")
        counts = self.counts + other.counts
        sumw2 = self.sum_w2 + other.sum_w2
        return Histogram(self.axes, counts, sumw2)

    @classmethod
    def read(cls, data):
        if not isinstance(data, dict):
            data = read_composite(data)
        axes = tuple(Axis.read(v) for v in data["axes"].values())
        return cls(axes, data["counts"], data["sumW2"])

    @property
    def numpy_dtype(self) -> np.dtype:
        return np.dtype(
            [
                (
                    "axes",
                    [(f"element{i}", a.numpy_dtype) for i, a in enumerate(self.axes)],
                ),
                ("counts", self.counts.dtype, self.counts.shape),
                ("sumW2", self.sum_w2.dtype, self.sum_w2.shape),
            ]
        )

    def to_numpy(self) -> np.ndarray:
        return np.asarray(
            (tuple(a.to_numpy() for a in self.axes), self.counts, self.sum_w2),
            dtype=self.numpy_dtype,
        )

    def write(self, file: h5py.Group, name: str):
        """Write the histogram to the provided H5 group with the provided name"""
        file.create_dataset(
            name, shape=(), dtype=self.numpy_dtype, data=self.to_numpy()
        )

    def plot(self, axes: plt.Axes = None, plot_errors=True, log=False, **kwargs):
        if axes is None:
            axes = plt.gca()
        if self.n_dims == 1:
            x_axis = self.axes[0]

            stairs = axes.stairs(
                edges=x_axis.edges,
                values=self.counts[x_axis.get_slice(False, False)],
                **kwargs,
            )
            if plot_errors:
                centers = [
                    x[1] + (x[0] - x[1]) / 2
                    for x in zip(x_axis.edges[1:], x_axis.edges[:-1])
                ]
                axes.errorbar(
                    centers,
                    self.counts[x_axis.get_slice(False, False)],
                    yerr=np.sqrt(self.sum_w2[x_axis.get_slice(False, False)]),
                    color="none",
                    ecolor=stairs.get_edgecolor(),
                    capsize=2,
                )
            x_axis.format_axis(axes, "x")
            if log:
                axes.set_yscale("log")
        elif self.n_dims == 2:
            kwargs.setdefault("shading", "flat")
            if log and "norm" not in kwargs:
                kwargs["norm"] = LogNorm(
                    vmin=kwargs.pop("vmin", None), vmax=kwargs.pop("vmax", None)
                )
            slices = tuple(a.get_slice(False, False) for a in self.axes)
            plot = axes.pcolormesh(
                self.axes[0].edges, self.axes[1].edges, self.counts[slices].T, **kwargs
            )
            self.axes[0].format_axis(axes, "x")
            self.axes[1].format_axis(axes, "y")
            axes.get_figure().colorbar(plot, ax=axes)
        else:
            raise ValueError("Can only plot 1 or 2D histograms")

    def cumulative(self, reverse: bool = False):
        if self.n_dims != 1:
            raise NotImplementedError("cumulative is only implemented for 1 dimension")

        new_counts = self.counts
        new_sumw2 = self.sum_w2
        if reverse:
            new_counts = new_counts[::-1]
            new_sumw2 = new_sumw2[::-1]
        new_counts = np.cumsum(new_counts)
        new_sumw2 = np.cumsum(new_sumw2)
        if reverse:
            new_counts = new_counts[::-1]
            new_sumw2 = new_sumw2[::-1]
        return Histogram(self.axes, new_counts, new_sumw2)
