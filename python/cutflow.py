import tabulate
import math
from pythanubis.geometry import Geometry
from pythanubis.analysis import calculate_boost
from pythanubis.axes import CategoryAxis
from pythanubis.histogram import Histogram
import numpy as np
import copy


def phi_mpi_pi(phi):
    return (phi + np.pi) % (2 * np.pi) - np.pi


def build_cutflow(
    selections,
    scenarios,
    df,
    initial_cutflow,
    detect_func,
    initial_weight=1.0,
    geometry=Geometry(),
    extra={},
):

    # create rescaled phi and mirrored theta branches
    rescaled_phi = geometry.rescale_phi(df["phi"]).to_numpy()
    theta = 2 * np.arctan(np.exp(-df["eta"])).to_numpy()
    mirrored_theta = geometry.mirror_theta(theta)
    met = np.sqrt(df["mpx"] * df["mpx"] + df["mpy"] * df["mpy"])
    met_phi = np.arctan2(df["mpy"], df["mpx"])
    boost = calculate_boost(df["pt"], df["eta"], df["mass"])

    sel_funcs = {
        "cosTheta": 0.5,
        "phi": geometry.rescaled_phi_weight(),
        "calo": np.exp(-9.7),
        "activeVolume": detect_func(mirrored_theta, rescaled_phi, boost),
        "met": met > 30,
        "metDPhi": np.abs(phi_mpi_pi(met_phi - df["phi"])) < 1.2,
        "0Jet": df["drClosestJet"] >= 0.5,
        "1Charged": df["nCharged"] <= 1,
        "0Charged": df["nCharged"] == 0,
        "0Jet0Charged": (df["drClosestJet"] >= 0.5) & (df["nCharged"] == 0),
    }
    sel_funcs.update(extra)

    cutflow = Histogram((CategoryAxis(" ", ["All", "$p_T$"] + selections + scenarios),))
    # Copy entries from the initial cutflow
    for cut in "All", "$p_T$":
        cutflow[cutflow.find_bin(cut)] = initial_cutflow[cut]

    weight = copy.copy(initial_weight)
    # if the initial weight is just a number make sure it's repeated across the length of the df
    if isinstance(weight, float):
        weight = np.repeat(weight, len(df))
    for sel in selections:
        weight *= sel_funcs[sel]
        cutflow[cutflow.find_bin(sel)] = (np.sum(weight), np.sum(weight * weight))

    for sel in scenarios:
        new_weight = weight * sel_funcs[sel]
        cutflow[cutflow.find_bin(sel)] = (
            np.sum(new_weight),
            np.sum(new_weight * new_weight),
        )
    return cutflow


class Modes:
    Values = 0
    Efficiencies = 1
    RelativeEfficiencies = 2


def print_cutflows(cutflows, mode, headers, fmt=".4g"):
    if isinstance(cutflows, (list, tuple)):
        if not all(
            c.axes[0].categories == cutflows[0].axes[0].categories for c in cutflows
        ):
            raise ValueError("All cutflows must have the same bins")
        categories = cutflows[0].axes[0].categories
    else:
        categories = cutflows.axes[0].categories
        cutflows = (cutflows,)

    if len(headers) == len(cutflows) - 1:
        headers = ["selection"] + headers

    if mode == Modes.Values:
        entry_fmt = f"{{count:{fmt}}} \xb1 {{error:{fmt}}}"
        table = []
        for selection in categories:
            table.append(
                [selection]
                + [
                    entry_fmt.format(
                        count=c[selection][0], error=math.sqrt(c[selection][1])
                    )
                    for c in cutflows
                ]
            )
        return tabulate.tabulate(table, headers=headers)
    elif mode == Modes.Efficiencies:
        entry_fmt = f"({{count:{fmt}}} \xb1 {{error:{fmt}}})%"
        norms = [c[categories[0]][0] / 100 for c in cutflows]
        table = []
        for selection in categories:
            table.append(
                [selection]
                + [
                    entry_fmt.format(
                        count=c[selection][0] / n, error=math.sqrt(c[selection][1]) / n
                    )
                    for c, n in zip(cutflows, norms)
                ]
            )
        return tabulate.tabulate(table, headers=headers)
    elif mode == Modes.RelativeEfficiencies:

        entry_fmt = f"({{count:{fmt}}} \xb1 {{error:{fmt}}})%"
        itr = iter(categories)
        sel = next(itr)
        previous = [c[sel][0] / 100 for c in cutflows]
        table = [[sel] + ["---"] * len(cutflows)]
        for sel in itr:
            table.append(
                [sel]
                + [
                    entry_fmt.format(
                        count=c[sel][0] / n, error=math.sqrt(c[sel][1]) / n
                    )
                    for c, n in zip(cutflows, previous)
                ]
            )
            previous = [c[sel][0] / 100 for c in cutflows]
        return tabulate.tabulate(table, headers=headers)
    else:
        raise ValueError("Invalid mode")
