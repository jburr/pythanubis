"""Class for drawing multiple 1D histograms on top of each other"""

from typing import Iterable, List, Dict, Optional
from pythanubis.histogram import Histogram
import matplotlib.pyplot as plt
import numpy as np

import logging

log = logging.getLogger(__name__)


class Stack:

    _label: str
    _set_label: bool
    _histograms: List[Histogram]
    _names: List[str]

    def __init__(self, hists: Iterable[Histogram], label: str = None):
        """Create the stack with an iterable of histograms and (optionally) an axis label"""
        self._label = None
        self.label = label
        self._histograms = []
        self._names = []
        for name, hist in hists.items():
            self.add_histogram(hist, name)

    @property
    def label(self) -> Optional[str]:
        """The axis label that will be printed"""
        if self._label is None:
            if len(self._histograms) == 0:
                return None
            else:
                return self._histograms[0].axes[0].label
        else:
            return self._label

    @label.setter
    def label(self, value):
        self._label = value

    def add_histogram(self, hist: Histogram, name: str):
        if hist.n_dims != 1:
            raise ValueError("Stacks can only be used with 1D histograms!")
        if len(self._histograms) != 0:
            # Check consistency
            if self._label is None and self.label != hist.axes[0].label:
                log.warning(
                    f"x-axis label of added histogram '{hist.axes[0].label}' does not match existing label '{self.label}'"
                )
            if self._histograms[0].axes[0].edges != hist.axes[0].edges:
                raise ValueError("Edges of histograms in stack do not match!")
        self._histograms.append(hist)
        self._names.append(name)

    def plot(self, axes: plt.Axes = None, log=False, **kwargs):
        if len(self._histograms) == 0:
            return
        if axes is None:
            axes = plt.gca()

        base = np.repeat(0.0, self._histograms[0].core_shape)
        edges = self._histograms[0].axes[0].edges
        for name, hist in zip(self._names, self._histograms):
            values = hist.counts[hist.axes[0].get_slice(False, False)]
            axes.stairs(
                edges=edges, values=values + base, baseline=base, label=name, **kwargs
            )
            base += values
        self._histograms[0].axes[0].format_axis(axes, "x")
        if self.label is not None:
            axes.set_xlabel(self.label)
        if log:
            axes.set_yscale("log")
