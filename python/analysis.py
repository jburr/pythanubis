import numpy as np

from pythanubis.geometry import Geometry


def detection_probability(expected_distance, entry, exit, start=0):
    """Calculate the probability to detect a particle in ANUBIS

    Arguments
    ---------
        expected_distance: The average distance of travel before the particle decays/interacts
        entry: How far the particle has traveled before entering the volume
        exit: How far the particle has traveled before leaving the volume
        start: [optional] The start point for the particle
    """
    prob = np.exp(-(entry - start) / expected_distance) - np.exp(
        -(exit - start) / expected_distance
    )
    # Replace nans with 0
    if isinstance(prob, np.ndarray):
        return np.where(np.isnan(prob), 0, prob)
    elif np.isnan(prob):
        return 0
    else:
        return prob


def decay_probability(boost, ctau, entry, exit, start=0):
    """Calculate the probability for a particle to decay inside ANUBIS

    Arguments
    ---------
        boost: The boost of the particle (gamma = E/m)
        ctau: The lifetime * c
        entry: How far the particle has traveled before entering ANUBIS
        exit: How far the particle has traveled when it leaves ANUBIS
        start: [optional] The start point for the particle
    """
    return detection_probability(boost * ctau, entry, exit, start)

def interaction_probability_incl_concrete(
    air_int_length,
    concrete_int_length,
    entry,
    exit,
    theta=None,
    phi=None,
    geometry: Geometry = None,
    atlas_exit=None,
    concrete_entry=None,
    concrete_exit=None
):
    """Calculate the probability for a particle to interact inside ANUBIS

    Arguments
    ---------
        air_int_length: The interaction length with air
        concrete_int_length: The interaction length with concrete
        entry: How far the particle has traveled before entering ANUBIS
        exit: How far the particle has traveled when it leaves ANUBIS
        theta: The angle to the z-axis
        phi: The azimuthal angle
        geometry: The description of the geometry
        atlas_exit: How far the particle has traveled when it leave ATLAS
        concrete_entry: How far the particle has traveled when it enters the concrete
        concrete_exit: How far the particle has traveled when it exits the concrete

        Either geometry or all three of atlas_exit, concrete_entry and concrete_exit must be
        provided
    """
    # delta_L is the length traveled inside ANUBIS
    delta_L = entry - exit
    # entry and exit could be nan or inf, in which case delta_L will be nan but it should be 0
    if isinstance(delta_L, np.ndarray):
        delta_L[np.isnan(delta_L)] = 0
    elif np.isnan(delta_L):
        delta_L = 0
    survival_prob = interaction_survival_probability_incl_concrete(
        air_int_length,
        concrete_int_length,
        entry,
        theta,
        phi,
        geometry,
        atlas_exit,
        concrete_entry,
        concrete_exit,
    )
    return survival_prob * (1 - np.exp(-delta_L / air_int_length) )

def interaction_survival_probability_incl_concrete(
    air_int_length,
    concrete_int_length,
    entry,
    theta=None,
    phi=None,
    geometry: Geometry = None,
    atlas_exit=None,
    concrete_entry=None,
    concrete_exit=None
):
    """Calculate the probability for a particle to survive interactions before reaching ANUBIS

    Arguments
    ---------
        air_int_length: The interaction length with air
        concrete_int_length: The interaction length with concrete
        entry: How far the particle has traveled before entering ANUBIS
        theta: The angle to the z-axis
        phi: The azimuthal angle
        geometry: The description of the geometry
        atlas_exit: How far the particle has traveled when it leaves ATLAS
        concrete_entry: How far the particle has traveled when it enters the concrete
        concrete_exit: How far the particle has traveled when it exits the concrete

        Either geometry or all three of atlas_exit, concrete_entry and concrete_exit must be
        provided
    """
    if atlas_exit is None:
        atlas_exit = geometry.atlas_intersection(theta)
    if concrete_entry is None or concrete_exit is None:
        concrete_entry, concrete_exit = geometry.l1l2_concrete(theta, phi)
    return np.exp(
        -(entry - concrete_exit + concrete_entry - atlas_exit) / air_int_length
        - (concrete_exit - concrete_entry) / concrete_int_length
    )

def interaction_probability(
    interaction_length, entry, exit, theta=None, phi=None, geometry: Geometry=None, atlas_exit=None
):
    """Calculate the probability for a particle to interact inside ANUBIS

    Arguments
    ---------
        interaction_length: The mean free path for the particle to interact
        entry: How far the particle has traveled before entering ANUBIS
        exit: How far the particle has traveled when it leaves ANUBIS
        theta: The angle to the z-axis
        phi: The azimuthal angle
        geometry: The description of the geometry
        atlas_exit: How far the particles has traveled when it leaves ATLAS

        Either geometry or atlas_exit must be provided
    """
    if atlas_exit is None:
        atlas_exit = geometry.atlas_intersection(theta)
    survival_prob = interaction_survival_probability(
        interaction_length,
        entry,
        theta,
        phi,
        geometry,
        atlas_exit
    )
    # delta_L is the length traveled inside ANUBIS
    delta_L = entry - exit
    # entry and exit could be nan or inf, in which case delta_L will be nan but it should be 0
    if isinstance(delta_L, np.ndarray):
        delta_L[np.isnan(delta_L)] = 0
    elif np.isnan(delta_L):
        delta_L = 0
    return survival_prob * (1 - np.exp(-delta_L / interaction_length) )

def interaction_survival_probability(
    interaction_length, entry, theta=None, phi=None, geometry: Geometry=None, atlas_exit=None
):
    """Calculate the probability for a particle to survive interactions before reaching ANUBIS

    Arguments
    ---------
        interaction_length: The mean free path for the particle to interact
        entry: How far the particle has traveled before entering ANUBIS
        theta: The angle to the z-axis
        phi: The azimuthal angle
        geometry: The description of the geometry
        atlas_exit: How far the particles has traveled when it leaves ATLAS

        Either geometry or atlas_exit must be provided
    """
    if atlas_exit is None:
        atlas_exit = geometry.atlas_intersection(theta)
    return np.exp(-(entry - atlas_exit)/interaction_length)


def calculate_boost(pt, eta, m):
    """Calculate the boost of a particle from its pt, eta and m"""
    p = pt * np.cosh(eta)
    e = np.sqrt(p * p + m * m)
    return e / m


def eta_to_theta(eta):
    return 2 * np.arctan(np.exp(-eta))


def theta_to_eta(theta):
    return -np.log(np.tan(theta / 2))
