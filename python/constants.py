# Take pion interaction length from https://pdg.lbl.gov/2020/AtomicNuclearProperties/HTML/air_dry_1_atm.html
# Then, following appendix B of https://cds.cern.ch/record/683812/files/tilecal-99-007.pdf multiply by 5/4 **very approximate**

import dataclasses
from typing import Optional

@dataclasses.dataclass(frozen=True)
class Constants:

    air_int_length: float
    concrete_int_length: float
    ctau: Optional[float]
    mass: float

constants = {
    "Neutron": Constants(
        air_int_length = 747.7, # [m]
        concrete_int_length = 0.4239, # [m]
        ctau = None,
        mass = 0.939565, # [GeV]
    ),
    "Kaon": Constants(
        air_int_length = 1266, # [m]
        concrete_int_length = 0.699, # [m]
        ctau = 15.337,
        mass = 0.497611, # [GeV]
    ),
}
# Add a few aliases
constants["neutrons"] = constants["Neutron"]
constants["K0Ls"] = constants["Kaon"]
