import argparse

parser = argparse.ArgumentParser(
    description="Plot all histograms in the provided file",
    epilog="Can produce either a directory structure of image files or a single PDF file",
)
parser.add_argument("-i", "--input", help="The input H5 file")
parser.add_argument(
    "-o", "--output", help="The output location, either a directory or a PDF file"
)
parser.add_argument(
    "-s", "--single-pdf", action="store_true", help="If true, output a single PDF file"
)
parser.add_argument(
    "-f",
    "--format",
    default="png",
    help="If not in single PDF mode, the output file type to produce",
)
parser.add_argument(
    "-d",
    "--subdirectory",
    default="Histograms",
    help="Subdirectory containing histograms",
)
parser.add_argument(
    "-k", "--keys", nargs="*", help="Optional list of keys to write out specifially"
)
parser.add_argument(
    "-l", "--log", action="store_true", help="Whether to output log plots"
)

args = parser.parse_args()

import h5py
from pythanubis.histogram import Histogram
import matplotlib.pyplot as plt
from matplotlib.figure import Figure


import os

if os.path.exists(args.output):
    raise ValueError(f"Output {args.output} already exists!")

if args.single_pdf:
    from matplotlib.backends.backend_pdf import PdfPages

    pdf = PdfPages(args.output)

    def savefig(figure: Figure, name: str):
        figure.suptitle(name)
        pdf.savefig(figure)


else:
    os.mkdir(args.output)

    def savefig(figure: Figure, name: str):
        dirname, _, filename = name.rpartition("/")
        fulldir = os.path.join(args.output, dirname)
        os.makedirs(fulldir, exist_ok=True)
        figure.savefig(f"{fulldir}/{filename}.{args.format}")


def iter_group(group: h5py.Group):
    for k, v in group.items():
        if isinstance(v, h5py.Group):
            for k2 in iter_group(v):
                yield "/".join([k, k2])
        else:
            yield k


with h5py.File(args.input) as fp:
    group = fp[args.subdirectory] if args.subdirectory else fp
    for key in iter_group(group):
        print(key)
        if args.keys and key not in args.keys:
            continue
        histogram = Histogram.read(group[key])
        fig, axes = plt.subplots()
        histogram.plot(axes, log=args.log)
        savefig(fig, key)
        plt.close(fig)


# If we opened a PDF file, close it
if args.single_pdf:
    pdf.close()
