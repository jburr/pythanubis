"""Class to simplify calculating decay/interaction probabilities along a particle trajectory"""

import sys
import functools
import numpy as np
from typing import Callable, Dict, Optional, Tuple, Union

from pythanubis.analysis import eta_to_theta
from pythanubis.geometry import Geometry

# Get the relevant caching decorator for this version of python
if sys.version_info < (3, 8):
    cache = functools.lru_cache(maxsize=None)
else:
    cache = functools.cache

ValueType = Union[np.ndarray, float]
L1L2Func = Callable[[ValueType, ValueType], Tuple[ValueType, ValueType]]

class Trajectory:
    """Provides helper methods to extract the physics information from a trajectory or set of
    trajectories

    The trajectory information can be provided as individual numbers or numpy arrays
    """
    def __init__(
            self,
            eta: ValueType,
            phi: ValueType,
            boost: ValueType,
            geometry: Geometry,
            air_int_length: float,
            concrete_int_length: float,
            ctau: Optional[float],
            l1l2_concrete: L1L2Func = None,
            l1l2s: Dict[str, L1L2Func] = None,
    ):
        self._shape = np.broadcast(eta, phi, boost).shape
        self._eta = np.broadcast_to(eta, self.shape)
        self._phi = np.broadcast_to(phi, self.shape)
        self._boost = np.broadcast_to(boost, self.shape)
        self._geometry = geometry
        self._air_int_length = air_int_length
        self._concrete_int_length = concrete_int_length
        self._ctau = ctau
        if l1l2_concrete is None:
            l1l2_concrete = geometry.l1l2_concrete
        self._f_l1l2_concrete = l1l2_concrete
        if l1l2s is None:
            l1l2s = {
                "CavernPlusShaft": geometry.l1l2_cavern_plus_shaft,
                "ShaftOnly": geometry.l1l2_shaft_only,
            }
        self._f_l1l2s = l1l2s

    @property
    def eta(self):
        return self._eta

    @property
    def phi(self):
        return self._phi

    @property
    def boost(self):
        return self._boost

    @property
    @cache
    def theta(self):
        return eta_to_theta(self.eta)

    @property
    def shape(self):
        return self._shape

    @property
    def geometry(self):
        return self._geometry

    @property
    def air_int_length(self):
        return self._air_int_length

    @property
    def concrete_int_length(self):
        return self._concrete_int_length

    @property
    def ctau(self):
        return self._ctau

    @property
    @cache
    def atlas_intersection(self):
        """The intersection point between the trajectory and ATLAS"""
        return self.geometry.atlas_intersection(self.theta)

    @property
    @cache
    def l1l2_concrete(self):
        """Entry, exit points to and from the concrete
        
        Will be (0, 0) if the trajectory doesn't enter the concrete where we're interested
        """
        return self._f_l1l2_concrete(self.theta, self.phi)

    @property
    @cache
    def concrete_entry(self):
        """Entry point to the concrete

        Will be 0 if the trajectory doesn't enter the concrete where we're interested
        """
        return self.l1l2_concrete[0]


    @property
    @cache
    def concrete_exit(self):
        """Exit point from the concrete

        Will be 0 if the trajectory doesn't enter the concrete where we're interested
        """
        return self.l1l2_concrete[1]

    @cache
    def l1l2(self, geo):
        """Get the entry, exit points for the specified geometry"""
        return self._f_l1l2s[geo](self.theta, self.phi)

    @cache
    def entry(self, geo: str):
        """Get the entry point for ANUBIS for the specified geometry"""
        return self.l1l2(geo)[0]


    @cache
    def exit(self, geo: str):
        """Get the entry point for ANUBIS for the specified geometry"""
        return self.l1l2(geo)[1]


    @cache
    def decay_survival(self, geo: str):
        """Probability that the particle has not decayed before reaching ANUBIS"""
        if self.ctau is None:
            return np.full(self.shape, 1)
        else:
            return np.exp(-self.entry(geo) / (self.ctau * self.boost))

    @cache
    def interaction_survival(self, geo: str, consider_concrete: bool=True):
        """Probability has not interacted before reaching ANUBIS

        If consider_concrete is True (default) any concrete on the trajectory will be considered
        correctly
        """
        if consider_concrete:
            air_path = self.concrete_entry - self.atlas_intersection + self.entry(geo) - self.concrete_exit
            concrete_path = self.concrete_exit - self.concrete_entry
            return np.exp(-air_path / self.air_int_length - concrete_path / self.concrete_int_length)
        else:
            return np.exp(-(self.entry(geo) - self.atlas_intersection) / self.air_int_length)

    @cache
    def survival(self, geo: str, consider_concrete: bool=True):
        """Probability that the particle makes it to ANUBIS

        Considers both decays and interactions. If consider_concrete is True (default) any concrete
        on the trajectory will be considered correctly
        """
        return self.decay_survival(geo) * self.interaction_survival(geo, consider_concrete)

    @cache
    def decay_prob(self, geo: str, consider_int: bool = True, consider_concrete: bool = True):
        """The probability for the particle to decay inside ANUBIS

        If consider_int is True (default) the probability to interact is also considered.
        If consider_concrete is True (default) any concrete on the trajectory will be considered
        correctly
        """
        if self.ctau is None:
            return np.full(self.shape, 0)
        delta_L = self.exit(geo) - self.entry(geo)
        lam_decay = 1 / (self.ctau * self.boost)
        if consider_int:
            survival = self.survival(geo, consider_concrete)
            lam_int = 1 / self.air_int_length
            lam = lam_decay + lam_int
            return survival * lam_decay / lam * (1 - np.exp(-delta_L * lam))
        else:
            return self.decay_survival(geo) * (1 - np.exp(-delta_L * lam_decay))

    @cache
    def interaction_prob(self, geo: str, consider_decay: bool = True, consider_concrete: bool = True):
        """The probability for the particle to interact inside ANUBIS

        If consider_decay is True (default) the probability to decay is also considered.
        If consider_concrete is True (default) any concrete on the trajectory will be considered
        correctly
        """
        delta_L = self.exit(geo) - self.entry(geo)
        lam_int = 1 / self.air_int_length
        if consider_decay and self.ctau is not None:
            survival = self.survival(geo, consider_concrete)
            lam_decay = 1 / (self.ctau * self.boost)
            lam = lam_decay + lam_int
            return survival * lam_int / lam * (1 - np.exp(-delta_L * lam))
        else:
            return self.interaction_survival(geo, consider_concrete) * (1 - np.exp(-delta_L * lam_int))

    @cache
    def total_detection_prob(self, geo: str, consider_concrete: bool = True):
        return self.decay_prob(geo, consider_concrete=consider_concrete) + self.interaction_prob(geo, consider_concrete=consider_concrete)
