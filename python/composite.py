import abc
from typing import Dict
import numpy as np

def read_composite(obj) -> Dict:
    return {
        name: read_composite(data) if isinstance(data, np.void) else data
        for name, data in zip(obj.dtype.names, obj[()])
    }

