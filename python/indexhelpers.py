import enum
from typing import Union, Tuple
from numbers import Real
from math import inf


class Range:
    def __init__(
        self,
        lower_value: Union[str, Real],
        lower_closed: bool,
        upper_value: Union[str, Real],
        upper_closed: bool,
    ):
        self._lower_value = lower_value
        self._upper_value = upper_value
        self._lower_closed = lower_closed
        self._upper_closed = upper_closed

    def __call__(self, axis) -> slice:
        lower_bin = axis.find_bin(self._lower_value)
        upper_bin = axis.find_bin(self._upper_value)
        if not self._lower_closed:
            lower_bin += 1
        if self._upper_closed:
            upper_bin += 1
        return slice(lower_bin, upper_bin)

    def __str__(self) -> str:
        return f"{self._lower_value} {'<=' if self._lower_closed else '<'} v {'<=' if self._upper_closed else '<'} {self._upper_value}"


class BinIndexer:
    class Mode(enum.Enum):
        """The type of comparison being done:

        LT: All bins below the one containing the value
        LE: All bins below and including the one containing the value
        EQ: The bin containing the value
        GE: All bins above and including the one containing the value
        GT: All bins above the one contianing the value
        """

        LT = enum.auto()
        LE = enum.auto()
        EQ = enum.auto()
        GE = enum.auto()
        GT = enum.auto()

    def __init__(self, value: Union[str, Real], mode: Mode):
        self._value = value
        self._mode = mode

    def __call__(self, axis) -> Union[int, slice]:
        bin = axis.find_bin(self._value)
        if self._mode == BinIndexer.Mode.LT:
            return slice(bin)
        elif self._mode == BinIndexer.Mode.LE:
            return slice(bin + 1)
        elif self._mode == BinIndexer.Mode.EQ:
            return bin
        elif self._mode == BinIndexer.Mode.GE:
            return slice(bin, None, None)
        elif self._mode == BinIndexer.Mode.GT:
            return slice(bin + 1, None, None)
        else:
            raise ValueError(f"Invalid mode {self._mode}")

    def __str__(self):
        if self._mode == BinIndexer.Mode.LT:
            op = "<"
        elif self._mode == BinIndexer.Mode.LE:
            op = "<="
        elif self._mode == BinIndexer.Mode.EQ:
            op = "=="
        elif self._mode == BinIndexer.Mode.GE:
            op = ">="
        elif self._mode == BinIndexer.Mode.GT:
            op = ">"
        else:
            raise ValueError(f"Invalid mode {self._mode}")
        return f"{op} {self._value}"

    def __lt__(self, value: Union[str, Real]) -> Range:
        if self._mode not in (BinIndexer.Mode.GE, BinIndexer.Mode.GT):
            raise NotImplementedError
        return Range(self._value, self._mode == BinIndexer.Mode.GE, value, False)

    def __le__(self, value: Union[str, Real]) -> Range:
        if self._mode not in (BinIndexer.Mode.GE, BinIndexer.Mode.GT):
            raise NotImplementedError
        return Range(self._value, self._mode == BinIndexer.Mode.GE, value, True)

    def __gt__(self, value: Union[str, Real]) -> Range:
        if self._mode not in (BinIndexer.Mode.LE, BinIndexer.Mode.LT):
            raise NotImplementedError
        return Range(value, False, self._value, self._mode == BinIndexer.Mode.LE)

    def __ge__(self, value: Union[str, Real]) -> Range:
        if self._mode not in (BinIndexer.Mode.LE, BinIndexer.Mode.LT):
            raise NotImplementedError
        return Range(value, True, self._value, self._mode == BinIndexer.Mode.GE)

    def __bool__(self):
        # Explicitly disallow conversion to bool to detect if a user is trying to make a range but
        # is actually going to get operator chaining...
        raise ValueError(
            "If trying to make a range, enclose one expression in brackets"
        )


class ValueProxy:
    def __lt__(self, value: Union[str, Real, Tuple[str, Real]]):
        if isinstance(value, tuple):
            return tuple(self < v for v in value)
        else:
            return BinIndexer(value, BinIndexer.Mode.LT)

    def __le__(self, value: Union[str, Real, Tuple[str, Real]]):
        if isinstance(value, tuple):
            return tuple(self <= v for v in value)
        else:
            return BinIndexer(value, BinIndexer.Mode.LE)

    def __eq__(self, value: Union[str, Real, Tuple[str, Real]]):
        if isinstance(value, tuple):
            return tuple(self == v for v in value)
        else:
            return BinIndexer(value, BinIndexer.Mode.EQ)

    def __ge__(self, value: Union[str, Real, Tuple[str, Real]]):
        if isinstance(value, tuple):
            return tuple(self >= v for v in value)
        else:
            return BinIndexer(value, BinIndexer.Mode.GE)

    def __gt__(self, value: Union[str, Real, Tuple[str, Real]]):
        if isinstance(value, tuple):
            return tuple(self > v for v in value)
        else:
            return BinIndexer(value, BinIndexer.Mode.GT)


val = ValueProxy()
full_range = -inf <= (val <= inf)
