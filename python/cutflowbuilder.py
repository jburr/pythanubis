import copy
import pandas
import numpy as np
from pandas.core.frame import DataFrame

from pythanubis.analysis import calculate_boost, eta_to_theta, detection_probability, interaction_survival_probability, interaction_survival_probability_incl_concrete
from pythanubis.axes import CategoryAxis
from pythanubis.geometry import Geometry
from pythanubis.histogram import Histogram


def phi_mpi_pi(phi):
    return (phi + np.pi) % (2 * np.pi) - np.pi


def join_particle_event(particles: pandas.DataFrame, events: pandas.DataFrame):
    """Join the particle and event dataframes"""
    return pandas.merge(
        particles,
        events,
        how="left",
        on="eventNumber",
        suffixes=(None, "_event"),
    )


def add_branch(df: pandas.DataFrame, name: str, value):
    """Add a new branch, raising an exception if it already exists"""
    if name in df:
        raise KeyError(f"Refusing to redefine branch '{name}'")
    df[name] = value


def add_standard_branches(df: pandas.DataFrame, geometry: Geometry):
    """Add the 'standard' branches in the cutflow

    These should be branches that _only_ depend on the geometry
    """
    add_branch(df, "rescaledPhi", geometry.rescale_phi(df["phi"]))
    add_branch(df, "theta", eta_to_theta(df["eta"]))
    add_branch(df, "mirroredTheta", geometry.mirror_theta(df["theta"]))
    add_branch(df, "boost", calculate_boost(df["pt"], df["eta"], df["mass"]))
    add_branch(df, "speed", np.sqrt(1 - df["boost"] ** -2))

    # Now add some selections/weights
    add_branch(
        df,
        "cosThetaWeight",
        0.5
        * (geometry.parameters.min_theta < df["mirroredTheta"])
        * (df["mirroredTheta"] < geometry.parameters.max_theta),
    )
    add_branch(df, "rescaledPhiWeight", geometry.rescaled_phi_weight())
    add_branch(df, "0Jet", df["drClosestJet"] >= 0.5)
    add_branch(df, "1Charged", df["nCharged"] <= 1)
    add_branch(df, "0Charged", df["nCharged"] == 0)
    add_branch(df, "0Jet0Charged", df["0Jet"] & df["0Charged"])
    add_branch(
        df, "ATLASIntersection", geometry.atlas_intersection(df["mirroredTheta"])
    )


def add_met_branches(df: pandas.DataFrame, suffix: str = ""):
    """Add met and metDPhi branches from the mpx and mpy branches

    If a suffix is provided it will be applied to the input and output branches
    """
    add_branch(
        df, f"met{suffix}", np.sqrt(df[f"mpx{suffix}"] ** 2 + df[f"mpy{suffix}"] ** 2)
    )
    add_branch(
        df, f"metPhi{suffix}", np.arctan2(df[f"mpy{suffix}"], df[f"mpx{suffix}"])
    )
    add_branch(df, f"metDPhi{suffix}", phi_mpi_pi(df["phi"] - df[f"metPhi{suffix}"]))


def add_entryexit(df: pandas.DataFrame, l1l2_func, suffix: str):
    """Add the entry/exit branches for a given geometry"""
    entry, exit = l1l2_func(
        df["mirroredTheta"].to_numpy(), df["rescaledPhi"].to_numpy()
    )
    add_branch(df, f"ANUBISEntry{suffix}", entry)
    add_branch(df, f"ANUBISExit{suffix}", exit)


def add_concrete_entryexit(df: pandas.DataFrame, geometry: Geometry):
    entry, exit = geometry.l1l2_concrete(
        df["mirroredTheta"].to_numpy(), df["rescaledPhi"].to_numpy()
    )
    add_branch(df, "ConcreteEntry", entry)
    add_branch(df, "ConcreteExit", exit)



def add_active_volume(df: pandas.DataFrame, detect_func, suffix: str):
    """Add the branch for the active volume decay probability for the given detect function"""
    add_branch(
        df,
        f"activeVolume{suffix}",
        detect_func(
            df["mirroredTheta"].to_numpy(),
            df["rescaledPhi"].to_numpy(),
            df["boost"].to_numpy(),
        ),
    )


def build_cutflow(
    selections, scenarios, weight, initial_cutflow, initial_cuts=["All", "$p_T$"]
):
    cutflow = Histogram(
        (
            CategoryAxis(
                " ",
                initial_cuts + list(selections) + list(scenarios),
            ),
        )
    )

    for cut in initial_cuts:
        cutflow[cut] = initial_cutflow[cut]

    # Make sure that we don't end up modifying something used elsewhere
    weight = copy.copy(weight)
    # Iterate through the selections applying them sequentially
    for sel_name, sel_weight in selections.items():
        weight *= np.where(np.isnan(sel_weight), 0, sel_weight)
        cutflow[sel_name] = (np.sum(weight), np.sum(weight ** 2))

    # Now iterate through the 'scenarios' and apply their weights individually
    for sel_name, sel_weight in scenarios.items():
        new_weight = weight * sel_weight
        cutflow[sel_name] = (np.sum(new_weight), np.sum(new_weight ** 2))
    return cutflow
