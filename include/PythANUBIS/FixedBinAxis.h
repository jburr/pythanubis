#ifndef PYTHANUBIS_FIXEDBINAXIS_H
#define PYTHANUBIS_FIXEDBINAXIS_H

#include "H5Composites/IBufferReader.h"
#include "H5Composites/IBufferWriter.h"
#include "H5Composites/H5Struct.h"

/**
 * @brief Representation for a class with bins of a fixed width
 */
class FixedBinAxis : public H5Composites::IBufferReader, public H5Composites::IBufferWriter
{
public:
    struct data_t {
        std::size_t nBins;
        double min;
        double max;
        H5COMPOSITES_INLINE_STRUCT_DTYPE(data_t, nBins, min, max);
    };

    using index_t = std::size_t;
    using value_t = double;

    FixedBinAxis(const H5::DataType& dtype);
    FixedBinAxis(const std::string& name, std::size_t nBins, double min, double max);

    static bool matchDType(const H5::DataType& dtype);

    void readBuffer(const void* buffer) override;
    void writeBuffer(void* buffer) const override;
    H5::DataType h5DType() const override;

    const std::string& name() const;
    std::size_t findBin(double value) const;
    index_t binOffset(double value) const { return findBin(value); }
    index_t binOffset(std::size_t index) const { return index; }
    std::size_t nBins() const;
    std::size_t fullNBins() const;

    double min() const;
    double max() const;
private:
    std::string m_name;
    data_t m_data;
    H5::CompType m_dtype;
}; //> end class FixedBinAxis

#endif //> !PYTHANUBIS_FIXEDBINAXIS_H