#pragma once
#include <cstddef>
#include <cmath>
#include "fastjet/JetDefinition.hh"

namespace PythANUBIS {
    using eventNum_t = std::size_t;
    
    
    static constexpr float minTheta = 0.828849;
    static constexpr float maxTheta = 1.51461;
    inline static const float minEta = -std::log(std::tan(maxTheta/2));
    inline static const float maxEta = -std::log(std::tan(minTheta/2));
    static constexpr float minKLPt = 3;
    static constexpr float minNeutronPt = 5.5;
    static constexpr float minChargedPt = 0.5;
    static constexpr float minJetPt = 10;
    static constexpr float maxChargedDR = 0.5;
    static constexpr float maxChargedDR2 = maxChargedDR*maxChargedDR;
    static constexpr int k0LId = 130;
    static constexpr int neutronId = 2112;
    static constexpr int LLPId = 35;
    static const fastjet::JetDefinition jetDef{
        fastjet::antikt_algorithm,
        0.4,
        fastjet::E_scheme,
        fastjet::Best
    };
}