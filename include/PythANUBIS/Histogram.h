#ifndef PYTHANUBIS_HISTOGRAM_H
#define PYTHANUBIS_HISTOGRAM_H

#include "H5Composites/IBufferReader.h"
#include "H5Composites/IBufferWriter.h"
#include "H5Composites/CompositeDefinition.h"
#include "H5Composites/TupleTraits.h"
#include "H5Composites/FixedLengthVectorTraits.h"
#include <tuple>
#include <vector>
#include <utility>

template <typename STORAGE, typename... AXES>
class Histogram : public H5Composites::IBufferReader, public H5Composites::IBufferWriter {
public:
    static const inline auto definition = H5Composites::CompositeDefinition<
        std::tuple<AXES...>, H5Composites::FLVector<STORAGE>, H5Composites::FLVector<STORAGE>>(
            {"axes", "counts", "sumW2"}
        );
    Histogram(const H5::DataType& dtype) :
        m_axes(definition.template init<0>(dtype.getId())),
        m_counts(definition.template init<1>(dtype.getId())),
        m_sumW2(definition.template init<2>(dtype.getId())),
        m_dtype(definition.createDType(m_axes, m_counts, m_sumW2)) {}

    Histogram(const AXES&... axes) :
        m_axes(axes...),
        m_counts(fullNBins()),
        m_sumW2(fullNBins()),
        m_dtype(definition.createDType(m_axes, m_counts, m_sumW2))
    {
        calculateStrides();
    }

    static constexpr std::size_t nDims() { return sizeof...(AXES); }

    template <std::size_t I>
    using axis_t = std::tuple_element_t<I, std::tuple<AXES...>>;

    void fill(const typename AXES::value_t&... values, const STORAGE& weight = 1)
    {
        std::size_t offset = offsetFromValues(values...);
        m_counts[offset] += weight;
        m_sumW2[offset] += weight*weight;
    }

    STORAGE& contents(const typename AXES::index_t&... indices) {
        return m_counts[offsetFromIndices(indices...)];
    }

    const STORAGE& contents(const typename AXES::index_t&... indices) const {
        return m_counts[offsetFromIndices(indices...)];
    }

    STORAGE& sumW2(const typename AXES::index_t&... indices) {
        return m_sumW2[offsetFromIndices(indices...)];
    }

    const STORAGE& sumW2(const typename AXES::index_t&... indices) const {
        return m_sumW2[offsetFromIndices(indices...)];
    }

    std::tuple<typename AXES::index_t...> findBin(const typename AXES::value_t&... values) const {
        return findBin(values..., std::make_index_sequence<sizeof...(AXES)>());
    }

    void readBuffer(const void* buffer) override {
        std::tie(m_axes, m_counts, m_sumW2) = definition.readAll(buffer, m_dtype);
        calculateStrides();
    }

    void writeBuffer(void* buffer) const override {
        definition.writeAll(buffer, m_dtype, m_axes, m_counts, m_sumW2);
    }

    H5::DataType h5DType() const override { return m_dtype; }

    template <std::size_t I>
    const axis_t<I>& axis() const { return std::get<I>(m_axes); }
    
    const std::tuple<AXES...>& axes() const { return m_axes; }

    std::array<std::size_t, sizeof...(AXES)> extent() const {
        return extent(std::make_index_sequence<sizeof...(AXES)>());
    }

    std::array<std::size_t, sizeof...(AXES)> fullExtent() const {
        return fullExtent(std::make_index_sequence<sizeof...(AXES)>());
    }

    std::size_t nBins() const {
        return nBins(std::make_index_sequence<sizeof...(AXES)>());
    }
    std::size_t fullNBins() const {
        return fullNBins(std::make_index_sequence<sizeof...(AXES)>());
    }
private:
    template <std::size_t... Is>
    std::array<std::size_t, sizeof...(AXES)> extent(
            std::index_sequence<Is...>) const {
        std::array<std::size_t, sizeof...(AXES)> value;
        (..., (value[Is] = std::get<Is>(m_axes).nBins()));
        return value;
    }

    template <std::size_t... Is>
    std::array<std::size_t, sizeof...(AXES)> fullExtent(
            std::index_sequence<Is...>) const {
        std::array<std::size_t, sizeof...(AXES)> value;
        (..., (value[Is] = std::get<Is>(m_axes).fullNBins()));
        return value;
    }

    template <std::size_t... Is>
    std::size_t nBins(std::index_sequence<Is...>) const {
        return (1 *...* std::get<Is>(m_axes).nBins());
    }

    template <std::size_t... Is>
    std::size_t fullNBins(std::index_sequence<Is...>) const {
        return (1 *...* std::get<Is>(m_axes).fullNBins());
    }

    template <std::size_t... Is>
    std::tuple<typename AXES::index_t...> findBin(
        const typename AXES::value_t&... values,
        std::index_sequence<Is...>) const
    {
        return std::make_tuple(std::get<Is>(m_axes).findBin(values)...);
    }

    std::size_t offsetFromValues(const typename AXES::value_t&... values) const
    {
        return offsetFromValues(values..., std::make_index_sequence<sizeof...(AXES)>());
    }
    
    template <std::size_t... Is>
    std::size_t offsetFromValues(
            const typename AXES::value_t&... values,
            std::index_sequence<Is...>) const
    {
        return (0 +...+ (std::get<Is>(m_axes).binOffset(values) * m_strides[Is]));
    }

    std::size_t offsetFromIndices(const typename AXES::index_t&... indices) const
    {
        return offsetFromIndices(indices..., std::make_index_sequence<sizeof...(AXES)>());
    }
    
    template <std::size_t... Is>
    std::size_t offsetFromIndices(
            const typename AXES::index_t&... indices,
            std::index_sequence<Is...>) const
    {
        return (0 +...+ (std::get<Is>(m_axes).binOffset(indices) * m_strides[Is]));
    }

    void calculateStrides()
    {
        setStride<0>();
    }

    template <std::size_t I>
    std::size_t setStride() {
        if constexpr (I + 1 == sizeof...(AXES))
            m_strides[I] = 1;
        else
            m_strides[I] = setStride<I+1>();
        return m_strides[I] * std::get<I>(m_axes).fullNBins();
    }

    std::tuple<AXES...> m_axes;
    std::array<std::size_t, sizeof...(AXES)> m_strides;
    std::vector<STORAGE> m_counts;
    std::vector<STORAGE> m_sumW2;
    H5::CompType m_dtype;
}; //> end class Histogram

template <typename STORAGE, typename... AXES>
Histogram<STORAGE, AXES...> makeHistogram(const AXES&... axes)
{
    return Histogram<STORAGE, AXES...>(axes...);
}
#endif //>! PYTHANUBIS_HISTOGRAM_H