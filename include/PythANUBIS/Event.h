#pragma once
#include "H5Cpp.h"
#include "PythANUBIS/defs.h"
#include "Pythia8/Event.h"
#include "fastjet/PseudoJet.hh"
#include "H5Composites/H5Struct.h"

/**
 * @brief Data struct for the output event type
 */

struct Event {
    PythANUBIS::eventNum_t eventNumber;
    std::size_t nParticles;
    float mpx;
    float mpy;
    double weight;
    H5COMPOSITES_INLINE_STRUCT_DTYPE(
        Event, eventNumber, nParticles, mpx, mpy, weight
    )
};

/**
 * @brief Data struct for the output particle data type
 */
struct Particle {
    Particle() = default;
    /**
     * @brief Fill basic information from a Pythia8 particle object
     */
    Particle(PythANUBIS::eventNum_t eventNumber, std::size_t particleNumber, const Pythia8::Particle& particle);
    PythANUBIS::eventNum_t eventNumber;
    std::size_t particleNumber;
    int PDGID;
    float pt;
    float eta;
    float phi;
    H5COMPOSITES_INLINE_STRUCT_DTYPE(
        Particle, eventNumber, particleNumber, PDGID, pt, eta, phi
    )
};

/**
 * @brief Data struct for the output jet data type
 */
struct Jet {
    Jet() = default;
    /**
     * @brief Fill basic information from a fastjet Pseudojet
     */
    Jet(PythANUBIS::eventNum_t eventNumber, std::size_t jetNumber, const fastjet::PseudoJet& pjet);
    PythANUBIS::eventNum_t eventNumber;
    std::size_t jetNumber;
    float pt;
    float eta;
    float phi;
    float m;
    std::size_t nParticles;
    H5COMPOSITES_INLINE_STRUCT_DTYPE(
        Jet, eventNumber, jetNumber, pt, eta, phi, m
    )
};