#ifndef PYTHANUBIS_CANDIDATEHELPER_H
#define PYTHANUBIS_CANDIDATEHELPER_H

#include "H5Composites/H5Struct.h"
#include "H5Composites/Handle.h"
#include "PythANUBIS/Histogram.h"
#include "PythANUBIS/CategoryAxis.h"
#include "PythANUBIS/FixedBinAxis.h"
#include "PythANUBIS/Event.h"
#include "H5Composites/TypedWriter.h"
#include "PythANUBIS/defs.h"
#include "Pythia8/Event.h"
#include "fastjet/PseudoJet.hh"
#include <vector>
#include <array>

struct Candidate
{
    PythANUBIS::eventNum_t eventNumber;
    int PDGID;
    float pt;
    float eta;
    float phi;
    float mass;
    float drClosestJet;
    float ptClosestJet;
    std::size_t nCharged;
    float mpx;
    float mpy;

    H5COMPOSITES_INLINE_STRUCT_DTYPE(
        Candidate, eventNumber, PDGID, pt, eta, phi, mass, drClosestJet, ptClosestJet, nCharged, mpx, mpy)
};

/**
 * @brief Helper class for writing information about a particular type of particle
 */
class CandidateHelper
{
public:
    /**
     * @brief Create the helper
     * @param output The H5::Group to write the dataframe to
     * @param histogramOutput The output H5::Group for histograms
     * @param name Name of the dataframe and prefix for histograms
     * @param minMET The minimum MET for the particles
     * @param outputAll If true, output all candidates without applying pt or theta cuts
     */
    CandidateHelper(H5::Group &output, H5::Group &histogramOutput, const std::string &name, float minMET, bool outputAll = false);
    CandidateHelper(CandidateHelper &&other) = default;
    /**
     * @brief Process a single candidate
     * @param event The struct object describing the current event
     * @param particle The candidate particle
     * @param charged The list of charged particles from the event
     * @param pjets The pseudojets in this event
     * @param pjetIdx The index of this particle in the pseudojet vector, SIZE_MAX if the particle
     *        did not form a pseudojet
     * @return Whether the candidate was written out
     */
    bool processCandidate(
        const Event &event,
        const Pythia8::Particle &particle,
        const std::vector<Pythia8::Particle> &charged,
        const std::vector<fastjet::PseudoJet> &pjets,
        std::size_t pjetIdx = SIZE_MAX);

private:
    float m_minMET;
    bool m_outputAll;
    H5Composites::TypedWriter<Candidate> m_writer;
    H5Composites::Handle<Histogram<float, CategoryAxis>> m_cutflow;
    H5Composites::Handle<Histogram<float, FixedBinAxis, FixedBinAxis>> m_ptEta;
    H5Composites::Handle<Histogram<float, FixedBinAxis, FixedBinAxis>> m_nCharged;
    H5Composites::Handle<Histogram<float, FixedBinAxis>> m_met;
}; //> end class CandidateHelper

#endif //> !PYTHANUBIS_CANDIDATEHELPER_H