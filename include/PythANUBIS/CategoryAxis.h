#ifndef PYTHANUBIS_CATEGORYAXIS_H
#define PYTHANUBIS_CATEGORYAXIS_H

#include "H5Composites/IBufferReader.h"
#include "H5Composites/IBufferWriter.h"
#include <vector>
#include <string>

/**
 * @brief Representation of a category axis
 * 
 * A category axis is an axis type used to represent data that can be binned into individual categories
 */
class CategoryAxis : public H5Composites::IBufferReader, public H5Composites::IBufferWriter
{
public:
    using index_t = std::string;
    using value_t = std::string;

    CategoryAxis(const H5::DataType &dtype);
    CategoryAxis(const std::string &name, const std::vector<std::string> &categories);

    static bool matchDType(const H5::DataType &dtype);

    void readBuffer(const void *buffer) override;
    void writeBuffer(void *buffer) const override;
    H5::DataType h5DType() const override;

    const std::string &name() const;
    std::string findBin(const std::string &name) const { return name; }
    std::size_t binOffset(const std::string &name) const;
    std::size_t nBins() const;
    std::size_t fullNBins() const;

    const std::string &category(std::size_t index) const;
    const std::vector<std::string> &categories() const;

private:
    std::string m_name;
    std::vector<std::string> m_categories;
    H5::CompType m_dtype;
}; //> end class CategoryAxis

#endif //> !PYTHANUBIS_CATEGORYAXIS_H