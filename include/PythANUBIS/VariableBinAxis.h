#ifndef PYTHANUBIS_VARIABLEBINAXIS_H
#define PYTHANUBIS_VARIABLEBINAXIS_H

#include "H5Composites/IBufferReader.h"
#include "H5Composites/IBufferWriter.h"
#include <vector>

/**
 * @brief Representation of an axis for data with variable bin sizes
 */
class VariableBinAxis : public H5Composites::IBufferReader, public H5Composites::IBufferWriter
{
public:
    using index_t = std::size_t;
    using value_t = double;

    VariableBinAxis(const H5::DataType& dtype);
    VariableBinAxis(const std::string& name, const std::vector<double>& edges);

    static bool matchDType(const H5::DataType& dtype);

    void readBuffer(const void* buffer) override;
    void writeBuffer(void* buffer) const override;
    H5::DataType h5DType() const override;

    const std::string& name() const;
    std::size_t findBin(double value) const;
    index_t binOffset(double value) const { return findBin(value); }
    index_t binOffset(std::size_t index) const { return index; }
    std::size_t nBins() const;
    std::size_t fullNBins() const;

    double min() const;
    double max() const;
    const std::vector<double>& edges() const;
private:
    std::string m_name;
    std::vector<double> m_edges;
    H5::CompType m_dtype;
}; //> end class VariableBinAxis

#endif //> !PYTHANUBIS_VARIABLEBINAXIS_H