#include "PythANUBIS/Event.h"

Particle::Particle(PythANUBIS::eventNum_t eventNumber, std::size_t particleNumber, const Pythia8::Particle& particle) :
    eventNumber(eventNumber),
    particleNumber(particleNumber),
    PDGID(particle.id()),
    pt(particle.pT()),
    eta(particle.eta()),
    phi(particle.phi())
{}

Jet::Jet(PythANUBIS::eventNum_t eventNumber, std::size_t jetNumber, const fastjet::PseudoJet& pjet) :
    eventNumber(eventNumber),
    jetNumber(jetNumber),
    pt(static_cast<float>(pjet.pt())),
    eta(static_cast<float>(pjet.eta())),
    phi(static_cast<float>(pjet.phi())),
    m(static_cast<float>(pjet.m())),
    nParticles(pjet.constituents().size())
{}