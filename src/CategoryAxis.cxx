#include "PythANUBIS/CategoryAxis.h"
#include "H5Composites/DTypes.h"
#include "H5Composites/FixedLengthVectorTraits.h"
#include "H5Composites/FixedLengthStringTraits.h"
#include "H5Composites/CompositeDefinition.h"
#include <numeric>
#include <algorithm>
#include <stdexcept>

namespace {
   static inline auto definition = H5Composites::CompositeDefinition<H5Composites::FLString, H5Composites::FLVector<H5Composites::FLString>>({"name", "categories"});
}

CategoryAxis::CategoryAxis(const H5::DataType& dtype) :
    m_dtype(dtype.getId())
{
    if (!matchDType(dtype))
        throw std::invalid_argument("Data type does not match category axis datatype");

    std::tie(m_name, m_categories) = definition.initAll(m_dtype);
}

CategoryAxis::CategoryAxis(const std::string& name, const std::vector<std::string>& categories) :
    m_name(name),
    m_categories(categories),
    m_dtype(definition.createDType(name, categories))
{}

bool CategoryAxis::matchDType(const H5::DataType& dtype)
{
    return definition.checkNames(dtype);
}

void CategoryAxis::readBuffer(const void* buffer)
{
    std::tie(m_name, m_categories) = definition.readAll(buffer, m_dtype);
}

void CategoryAxis::writeBuffer(void* buffer) const
{
    definition.writeAll(buffer, m_dtype, m_name, m_categories);
}

H5::DataType CategoryAxis::h5DType() const {
    return m_dtype;
}

const std::string& CategoryAxis::name() const {
    return m_name;
}

std::size_t CategoryAxis::binOffset(const std::string& name) const {
    auto itr = std::find(m_categories.begin(), m_categories.end(), name);
    return std::distance(m_categories.begin(), itr);
}

std::size_t CategoryAxis::nBins() const {
    return m_categories.size();
}

std::size_t CategoryAxis::fullNBins() const {
    return m_categories.size() + 1;
}

const std::string& CategoryAxis::category(std::size_t index) const {
    return m_categories.at(index);
}

const std::vector<std::string>& CategoryAxis::categories() const {
    return m_categories;
}