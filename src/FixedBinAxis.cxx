#include "PythANUBIS/FixedBinAxis.h"
#include "H5Composites/RWTraits.h"
#include "H5Composites/FixedLengthStringTraits.h"
#include "H5Composites/CompositeDefinition.h"
#include <stdexcept>

namespace {
    static inline auto definition = H5Composites::CompositeDefinition<H5Composites::FLString, FixedBinAxis::data_t>({"name", "data"});
}

FixedBinAxis::FixedBinAxis(const H5::DataType& dtype) :
    m_dtype(dtype.getId())
{
    if (!matchDType(dtype))
        throw std::invalid_argument("Incorrect datatype for a FixedBinAxis"); 
    m_name = definition.init<0>(m_dtype);
}

FixedBinAxis::FixedBinAxis(const std::string& name, std::size_t nBins, double min, double max) :
    m_name(name), m_data({nBins, min, max}), m_dtype(definition.createDType(m_name, m_data))
{}

bool FixedBinAxis::matchDType(const H5::DataType& dtype)
{
    return definition.checkNames(dtype);
}

void FixedBinAxis::readBuffer(const void* buffer)
{
    std::tie(m_name, m_data) = definition.readAll(buffer, m_dtype);
}

void FixedBinAxis::writeBuffer(void* buffer) const
{
    definition.writeAll(buffer, m_dtype, m_name, m_data);
}

H5::DataType FixedBinAxis::h5DType() const
{
    return m_dtype;
}

const std::string& FixedBinAxis::name() const
{
    return m_name;
}

std::size_t FixedBinAxis::findBin(double value) const
{
    if (value < m_data.min)
        return 0;
    else if (value >= m_data.max)
        return m_data.nBins + 1;
    else
    {
        double stride = (m_data.max - m_data.min)/m_data.nBins;
        return static_cast<std::size_t>((value-m_data.min)/stride) + 1;
    }
}

std::size_t FixedBinAxis::nBins() const
{
    return m_data.nBins;
}

std::size_t FixedBinAxis::fullNBins() const
{
    return m_data.nBins + 2;
}

double FixedBinAxis::min() const
{
    return m_data.min;
}

double FixedBinAxis::max() const
{
    return m_data.max;
}