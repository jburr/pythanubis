#include "PythANUBIS/VariableBinAxis.h"
#include "H5Composites/FixedLengthVectorTraits.h"
#include "H5Composites/FixedLengthStringTraits.h"
#include "H5Composites/CompositeDefinition.h"
#include <algorithm>
#include <stdexcept>

namespace {
    static auto inline definition = H5Composites::CompositeDefinition<H5Composites::FLString, H5Composites::FLVector<double>>({"name", "edges"});
}


VariableBinAxis::VariableBinAxis(const H5::DataType& dtype) :
    m_dtype(dtype.getId())
{
    if (!matchDType(dtype))
        throw std::invalid_argument("Incorrect datatype for a VariableBinAxis");
    
    std::tie(m_name, m_edges) = definition.initAll(m_dtype);
}

VariableBinAxis::VariableBinAxis(const std::string& name, const std::vector<double>& edges) :
    m_name(name), m_edges(edges), m_dtype(definition.createDType(name, edges))
{
    if (!std::is_sorted(edges.begin(), edges.end()))
        throw std::invalid_argument("Bin edges must be given in ascending order");
}

bool VariableBinAxis::matchDType(const H5::DataType& dtype)
{
    return definition.checkNames(dtype);
}

void VariableBinAxis::readBuffer(const void* buffer)
{
    std::tie(m_name, m_edges) = definition.readAll(buffer, m_dtype);
    if (!std::is_sorted(m_edges.begin(), m_edges.end()))
        throw std::invalid_argument("Bin edges must be given in ascending order");
}

void VariableBinAxis::writeBuffer(void* buffer) const
{
    definition.writeAll(buffer, m_dtype, m_name, m_edges);
}

H5::DataType VariableBinAxis::h5DType() const
{
    return m_dtype;
}

const std::string& VariableBinAxis::name() const
{
    return m_name;
}

std::size_t VariableBinAxis::findBin(double value) const
{
    auto itr = std::upper_bound(m_edges.begin(), m_edges.end(), value);
    return std::distance(m_edges.begin(), itr);
}

std::size_t VariableBinAxis::nBins() const
{
    return m_edges.size() - 1;
}

std::size_t VariableBinAxis::fullNBins() const
{
    return m_edges.size() + 1;
}

double VariableBinAxis::min() const
{
    return m_edges.front();
}

double VariableBinAxis::max() const
{
    return m_edges.back();
}

const std::vector<double>& VariableBinAxis::edges() const
{
    return m_edges;
}