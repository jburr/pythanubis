#include "PythANUBIS/CandidateHelper.h"
#include "fastjet/ClusterSequence.hh"
#include <limits>

namespace
{
    static constexpr float pi = 3.141592653589793238462643383279502884;
    float dPhi(float phi1, float phi2)
    {
        return std::fmod(pi + phi1 - phi2, 2 * pi) - pi;
    }
    float dR(float eta1, float eta2, float phi1, float phi2)
    {
        float deltaEta = eta1 - eta2;
        float deltaPhi = dPhi(phi1, phi2);
        return std::sqrt(deltaEta * deltaEta + deltaPhi * deltaPhi);
    }
    bool withinDR(float eta1, float eta2, float phi1, float phi2, float dr, float dr2)
    {
        float deta = eta1 - eta2;
        if (deta > dr)
            return false;
        float dphi = dPhi(phi1, phi2);
        if (dphi > dr)
            return false;
        return (deta * deta + dphi * dphi) < dr2;
    }
}

CandidateHelper::CandidateHelper(
        H5::Group& output,
        H5::Group& histogramOutput,
        const std::string& name,
        float minMET,
        bool outputAll) :
    m_minMET(minMET),
    m_outputAll(outputAll),
    m_writer(output, name),
    m_cutflow(
        histogramOutput, name+"Cutflow", makeHistogram<float>(
            CategoryAxis(" ", {
                "All",
                "$E_T^{\\mathrm{miss}}$"
            }))),
    m_ptEta(
        histogramOutput, name+"PtEta", makeHistogram<float>(
            FixedBinAxis("$p_T$ [GeV]", 500, 0, 50),
            FixedBinAxis("$\\eta$", 20, -5, 5))),
    m_nCharged(
        histogramOutput, name+"NCharged", makeHistogram<float>(
            FixedBinAxis("$p_T$ [GeV]", 9, 0.5, 5),
            FixedBinAxis("$\\Delta R$", 10, 0, 1))),
    m_met(
        histogramOutput, name+"Met", makeHistogram<float>(
            FixedBinAxis("$E_T^{\\mathrm{miss}}$ [GeV]", 100, 0, 100)
        )
    )
{}

bool CandidateHelper::processCandidate(
    const Event &event,
    const Pythia8::Particle &particle,
    const std::vector<Pythia8::Particle> &charged,
    const std::vector<fastjet::PseudoJet> &pjetsIn,
    std::size_t pjetIdx)
{
    float weight = event.weight;
    m_cutflow->fill("All", weight);
    m_ptEta->fill(particle.pT(), particle.eta(), weight);

    bool pass = true;

    Candidate candidate;
    candidate.eventNumber = event.eventNumber;
    candidate.PDGID = particle.id();
    candidate.pt = static_cast<float>(particle.pT());
    candidate.eta = static_cast<float>(particle.eta());
    candidate.phi = static_cast<float>(particle.phi());
    candidate.mass = static_cast<float>(particle.m());
    candidate.drClosestJet = std::numeric_limits<float>::max();
    candidate.ptClosestJet = -1;
    candidate.nCharged = 0;
    candidate.mpx = event.mpx;
    candidate.mpy = event.mpy;

    if (particle.isVisible())
    {
        // This was a visible particle we want to *treat* as invisible
        candidate.mpx += static_cast<float>(particle.px());
        candidate.mpy += static_cast<float>(particle.py());
    }

    float met = std::sqrt(candidate.mpx * candidate.mpx + candidate.mpy * candidate.mpy);
    m_met->fill(met, weight);

    pass &= met >= m_minMET;
    if (pass)
        m_cutflow->fill("$E_T^{\\mathrm{miss}}$", weight);
    else if (!m_outputAll)
        return false;

    // Get the number of charged particles
    for (const Pythia8::Particle &c : charged)
    {
        float deltaR = dR(c.eta(), candidate.eta, c.phi(), candidate.phi);
        if (deltaR < 0.5)
            ++candidate.nCharged;
        m_nCharged->fill(c.pT(), deltaR);
    }

    // Now rebuild the jets and MET
    std::vector<fastjet::PseudoJet> pjets;
    if (pjetIdx == SIZE_MAX)
    {
        // This was an invisible particle
        pjets = pjetsIn;
    }
    else
    {
        // This was a visible particle we want to *treat* as invisible
        pjets.reserve(pjetsIn.size());
        pjets.insert(pjets.end(), pjetsIn.begin(), pjetsIn.begin() + pjetIdx);
        pjets.insert(pjets.end(), pjetsIn.begin() + pjetIdx + 1, pjetsIn.end());
    }
        
    fastjet::ClusterSequence clusterSeq(pjets, PythANUBIS::jetDef);
    std::vector<fastjet::PseudoJet> jets = fastjet::sorted_by_pt(
        clusterSeq.inclusive_jets(PythANUBIS::minJetPt));
    // Find the closest jet
    for (const fastjet::PseudoJet &jet : jets)
    {
        float dr = dR(
            static_cast<float>(jet.eta()),
            candidate.eta,
            static_cast<float>(jet.phi()),
            candidate.phi);
        if (dr < candidate.drClosestJet)
        {
            candidate.drClosestJet = dr;
            candidate.ptClosestJet = static_cast<float>(jet.pt());
        }
    }

    m_writer.write(candidate);

    return true;
}