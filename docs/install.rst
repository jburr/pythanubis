=========================
Installation Instructions
=========================

.. contents:: Table of Contents

---------------------
Checking out the Code
---------------------

This repository depends on a submodule (from my personal GitHub) called H5Composites. In order to fully clone the repository remember to include the ``--recursive`` flag
::
    git clone ssh://git@gitlab.cern.ch:7999/jburr/pythanubis.git --recursive

(NB: This assumes you have set up your ssh key beforehand).
You should also create a separate build and run directories, so your directory structure should look like::
    build pythanubis run

Cambridge Machines
------------------

These instructions are for setting up and running the code on the Cambridge computing systems, or indeed anything that has ``cvmfs`` mounted.
Once you the code checked out call::
    setupATLAS
    lsetup "views LCG_101 x86_64-centos7-gcc10-opt"
    export PYTHIA8_ROOT_DIR=${PYTHIA8}
    export LHAPDF_DATA_PATH=/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/:/cvmfs/sft.cern.ch/lcg/releases/MCGenerators/lhapdf/6.3.0-ff26e/x86_64-centos7-gcc8-opt/share/LHAPDF/

I keep these files in a ``setup.sh`` file. Note that this assumes you have the standard ``setupATLAS`` alias defined in your shell.  

The first time you checkout and build the code you will need to navigate to the ``build`` directory and call
::
    cmake ../pythanubis
    make
    . setup.sh

After this you will only need to call ``make`` or ``cmake`` if you make changes to the code. The ``build/setup.sh`` script is created by the cmake code and should be sourced at the start of every session (it sets up some environment variables to make the executables and python modules findable).

You will need to install several python modules to run the python code. These setup steps must be done with the same python3 version that you will use to run the code
::
    python3 -m pip install --user pandas numpy matplotlib tables h5py tabulate

Personal Machine
----------------

These instructions are for setting up and running the code on a personal machine without cvmfs installed. The software in this package relies on a few extra packages that you will need to install. For all the following instructions, where there is a ``configure`` script I would personally recommend that you install these to ``/usr/local`` using the ``--prefix`` option. Note this means that when you call ``make install`` you will have to actually call ``sudo make install`` to run with the correct permissions.

LHAPDF:
    This package provides some of the PDFs that we use. To install follow the instructions `here <https://lhapdf.hepforge.org/install.html>`_. 
    The latest version when I installed it was 6.4.0 so replace 6.X.Y with 6.4.0 in the instructions.
    You will also need to install the ``MSTW2008lo68cl`` PDF set. To do that, download the relevant tarball from `here <https://lhapdf.hepforge.org/pdfsets.html>`_ and unpack it into the ``/usr/local/share/LHAPDF`` folder (assuming you used the prefix as recommended). Afterwards this folder should contain a ``MSTW2008lo68cl`` folder and two files called ``lhapdf.conf`` and ``pdfsets.index``.

Pythia:
    This is the event generation software used for creating hadronic showers from input LHE files or for performing full event generation from scratch. You can download and install the latest version from https://pythia.org/. When installing it, you will need to configure it to point at your LHAPDF installation using the ``--with-lhapdf6=/usr/local`` (assuming that is where you installed it). You will also need to add the line ``export PYTHIA8_ROOT_DIR=/usr/local`` to your ``.bashrc`` file to help CMake find these libraries.

FastJet:
    This provides methods to recluster and produce jets. You can install it following the instructions `here <http://fastjet.fr/quickstart.html>`_.

CMakeTools:
    This is used to help provide CMake commands for many HEP-related packages. First clone the `git repository <https://github.com/HSF/cmaketools>`_. Then to tell our CMake code how to find it add the line ``export CMAKETOOLS_LOC=/path/to/cmaketools`` to your ``.bashrc`` file.

HDF5:
    This is the library used to create the output format (H5). To install use ``sudo apt-get install libhdf5-serial-dev`` if using a linux-based system like Ubuntu or ``brew install hdf5`` if using macOS.

Boost:
    This is a collection of useful extra C++ libraries. It's so commonly used it *may* already be installed on your system. To install use ``sudo apt-get install libboost-all-dev`` on linux or ``brew install boost`` on macOS.

You can now build the code.
::
    cmake ../pythanubis
    make
    . setup.sh

You will also need to install the same set of python3 packages as for the Cambridge version. Given that this is your personal machine however you can skip the user flag.
::
    python3 -m pip install pandas numpy matplotlib tables h5py tabulate

------------------
Running a Test Job
------------------

To test that everything works, run the following commands (ideally from the ``run`` directory)
::
    gen2 --nEvent 1000 --output minbias.h5 --settings ../pythanubis/data/minbias.txt
    python3 -m pythanubis.plot_all --input minbias.h5 --output minbias.pdf --single-pdf --log

If this has worked successfully, the first command should produce a h5 containing some histograms and some dataframes and the second command should plot all the histograms into a PDF file.
