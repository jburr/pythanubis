#include "H5Composites/MergeRules.h"
#include "H5Composites/MergeUtils.h"
#include "H5Composites/SmartBuffer.h"
#include "PythANUBIS/Histogram.h"
#include "H5Cpp.h"
/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "H5Cpp.h"
#include <boost/program_options.hpp>
#include <boost/algorithm/string/split.hpp> 
#include <boost/algorithm/string/trim.hpp> 
#include <iostream>
#include <iomanip>
#include <algorithm>

namespace {
    using namespace H5Composites;
    bool mergeHistogram(
        void* target, const H5::CompType& targetDType,
        const void* source, const H5::CompType& sourceDType)
    {
        if (!match(
            target, targetDType.getMemberDataType(0),
            source, sourceDType.getMemberDataType(0)))
            return false;
        sumInto(
            static_cast<unsigned char*>(target) + targetDType.getMemberOffset(1),
            targetDType.getMemberDataType(1),
            static_cast<const unsigned char*>(source) + sourceDType.getMemberOffset(1),
            sourceDType.getMemberDataType(1)
        );
        sumInto(
            static_cast<unsigned char*>(target) + targetDType.getMemberOffset(2),
            targetDType.getMemberDataType(2),
            static_cast<const unsigned char*>(source) + sourceDType.getMemberOffset(2),
            sourceDType.getMemberDataType(2)
        );
        return true;
    }
}

/**
 * A simple script to merge HDF5 files.
 *
 * This script is intended to read in a list of HDF5 files and create a new file
 * with all datasets contained inside them concatenated along a particular axis.
 */


int main(int argc, char* argv[]) {
  // The options
  std::string outputFile = "merged.h5";
  std::string inCSV = "";
  std::vector<std::string> inputFiles;
  hsize_t mergeAxis = 0;
  int chunkSize = -1;
  bool requireSameFormat = true;
  std::size_t bufferSizeMB = 100;
  std::size_t bufferSizeRows = -1;
  bool overwrite = false;
  std::vector<std::string> keys;

  namespace po = boost::program_options;
  po::options_description desc("Allowed options");
  desc.add_options()
    ("output,o", po::value(&outputFile), "The output file.")
    ("input,i", po::value(&inCSV), "A comma separated list of input files")
    ("allowDifferentFormats", po::bool_switch(&requireSameFormat),
     "Allow input files to have different formats.")
    ("mergeAxis,a", po::value(&mergeAxis),
     "The axis along which to merge datasets")
    ("chunkSize,c", po::value(&chunkSize),
     "The chunk size to use along the merge axis. If left negative uses the same chunks as the first input.")
    ("bufferSizeMB,B", po::value(&bufferSizeMB),
     "The size of the buffer to use in MB. Cannot be set with 'bufferSizeRows'")
    ("bufferSizeRows,b", po::value(&bufferSizeRows),
     "The size of the buffer to use in rows. Cannot be set with 'bufferSizeMB'")
    ("overwrite,w", po::bool_switch(&overwrite),
     "Overwrite the output file if it already exists. Cannot be set with 'in-place'")
    ("keys,k", po::value(&keys), "If set, only output these keys")
    ("help,h", "Print this message and exit.");

  po::options_description hidden;
  hidden.add_options()
    ("inputFiles", po::value(&inputFiles), "The input files");
  po::positional_options_description positional;
  positional.add("inputFiles", -1); //> All positional arguments are input files

  po::variables_map vm;
  po::options_description allOptions;
  allOptions.add(desc).add(hidden);

  po::store(
      po::command_line_parser(argc, argv).
        options(allOptions).
        positional(positional).
        run(),
      vm);
  // Do help before notify - notify will verify input arguments which we don't
  // want to do with help
  if (vm.count("help") ) {
    std::cout << "Merge HDF5 files. Usage:" << std::endl << std::endl;
    std::cout << "merge [options] [--input input1,input2,... | input1 [input2 ...]]" << std::endl << std::endl;
    std::cout << desc << std::endl;
    return 0;
  }
  po::notify(vm);

  if (inCSV.size() > 0) {
    std::vector<std::string> splitCSV;
    boost::algorithm::split(splitCSV, inCSV, boost::algorithm::is_any_of(",") );
    for (const std::string& i : splitCSV)
      inputFiles.push_back(boost::algorithm::trim_copy(i) );
  }
  if (inputFiles.size() == 0) {
    std::cerr << "You must specify at least 1 input file!" << std::endl;
    return 1;
  }
  if (vm.count("bufferSizeMB") && vm.count("bufferSizeRows") ) {
    std::cerr << "You cannot specify both bufferSizeMB and bufferSizeRows!" << std::endl;
    return 1;
  }
  std::size_t bufferSize;
  bool bufferInRows;
  if (vm.count("bufferSizeRows") ) {
    bufferSize = bufferSizeRows;
    bufferInRows = true;
  }
  else {
    // Default used if neither was set or if bufferSizeMB is set
    static constexpr std::size_t MB = 1024*1024;
    if (std::size_t(-1) / bufferSizeMB < MB)
      throw std::overflow_error(
          "Requested buffer size would overflow the register!");
    bufferSize = bufferSizeMB * MB;
    bufferInRows = false;
  }
  // Make the output file
  H5::H5File fOut(outputFile, overwrite ? H5F_ACC_TRUNC : H5F_ACC_EXCL);
  // Loop over the input files and merge them
  for (const std::string& inName : inputFiles) {
    std::cout << "Merging file " << inName << std::endl;
    H5::H5File fIn(inName, H5F_ACC_RDONLY);

    for (std::size_t idx = 0; idx < fIn.getNumObjs(); ++idx)
    {
        std::string childName = fIn.getObjnameByIdx(idx);
        if (keys.size() != 0 && std::find(keys.begin(), keys.end(), childName) == keys.end())
          continue;
        // Find the correct index in the target
        hsize_t targetIdx = 0;
        for (; targetIdx < fOut.getNumObjs(); ++targetIdx) 
            if (fOut.getObjnameByIdx(targetIdx) == childName)
                break;
        bool found = targetIdx != fOut.getNumObjs();
        if (childName == "Histograms")
        {
            H5::Group sourceGroup = fIn.openGroup(childName);
            if (found)
            {
                H5::Group targetGroup = fOut.openGroup(childName);
                for (std::size_t idx2 = 0; idx2 < sourceGroup.getNumObjs(); ++idx2)
                {
                    std::string childName = sourceGroup.getObjnameByIdx(idx2);
                    // Find the correct index in the target
                    hsize_t targetIdx = 0;
                    for (; targetIdx < targetGroup.getNumObjs(); ++targetIdx) 
                        if (targetGroup.getObjnameByIdx(targetIdx) == childName)
                            break;
                    bool found = targetIdx != targetGroup.getNumObjs();
                    if (!found) throw std::invalid_argument("Histogram not found");
                    H5::DataSet target = targetGroup.openDataSet(childName);
                    H5::DataSet source = sourceGroup.openDataSet(childName);
                    H5Composites::SmartBuffer targetBuffer(target.getDataType().getSize());
                    H5Composites::SmartBuffer sourceBuffer(source.getDataType().getSize());
                    target.read(targetBuffer.get(), target.getDataType());
                    source.read(sourceBuffer.get(), source.getDataType());
                    if (!mergeHistogram(targetBuffer.get(), target.getDataType().getId(), sourceBuffer.get(), source.getDataType().getId()))
                        throw std::invalid_argument("Failed to merge histogram");
                    target.write(targetBuffer.get(), target.getDataType());
                }
            }
            else
            {
                H5::Group targetGroup = fOut.createGroup(childName);
                for (std::size_t idx2 = 0; idx2 < sourceGroup.getNumObjs(); ++idx2)
                {
                    std::string childName = sourceGroup.getObjnameByIdx(idx2);
                    H5::DataSet histogram = sourceGroup.openDataSet(childName);
                    H5::DataSet target = targetGroup.createDataSet(
                        childName,
                        histogram.getDataType(),
                        H5::DataSpace(),
                        histogram.getCreatePlist()
                    );
                    H5Composites::SmartBuffer buffer(histogram.getDataType().getSize());
                    histogram.read(buffer.get(), histogram.getDataType());
                    target.write(buffer.get(), histogram.getDataType());
                }
            }
        }
        else {
            H5::DataSet source = fIn.openDataSet(childName);
            if (found)
            {
                H5::DataSet target = fOut.openDataSet(childName);
                H5Composites::extendDataset(target, source, mergeAxis, bufferSize);
            }
            else
            {
                
                H5::DataSpace sourceSpace = source.getSpace();
                // Get the new extent
                std::vector<hsize_t> DSExtent(sourceSpace.getSimpleExtentNdims(), 0);
                sourceSpace.getSimpleExtentDims(DSExtent.data() );
                // Set the merge axis to be 0 length to begin with
                //DSExtent.at(mergeAxis) = 0;
                std::vector<hsize_t> maxDSExtent = DSExtent;
                maxDSExtent.at(mergeAxis) = H5S_UNLIMITED;

                H5::DataSet target = fOut.createDataSet(
                    childName,
                    source.getDataType(),
                    H5::DataSpace(DSExtent.size(), DSExtent.data(), maxDSExtent.data()),
                    source.getCreatePlist()
                );
                H5Composites::SmartBuffer buffer(source.getStorageSize());
                source.read(buffer.get(), source.getDataType());
                target.write(buffer.get(), source.getDataType());
            }
            
        }
    }
  }

  return 0;
}
