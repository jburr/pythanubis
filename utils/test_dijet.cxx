#include "PythANUBIS/defs.h"
#include "PythANUBIS/Histogram.h"
#include "PythANUBIS/Histogram.h"
#include "PythANUBIS/FixedBinAxis.h"
#include "PythANUBIS/CategoryAxis.h"
#include "PythANUBIS/CandidateHelper.h"
#include "Pythia8/Pythia.h"
#include "H5Cpp.h"
#include <iostream>
#include <boost/program_options.hpp>

namespace {
    template <typename... AXES>
    auto addHistogram(H5::Group& group, const std::string& name, const AXES&... axes)
    {
        return H5Composites::makeHandle(group, name, makeHistogram<float>(axes...));
    }
}

int main(int argc, const char* argv[])
{
    namespace po = boost::program_options;
    std::size_t nEvents;
    std::size_t seed;
    std::string outputFile;
    double pTHatMin;
    bool overwrite = false;
    po::options_description allOptions("Allowed options");
    allOptions.add_options()
        ("nEvents,n", po::value(&nEvents)->default_value(10000), "The number of events to produce")
        ("randomSeed,r", po::value(&seed)->default_value(1), "The random seed to use")
        ("pTHatMin,p", po::value(&pTHatMin)->default_value(0), "The pythia pTHatMin value")
        ("output,o", po::value(&outputFile)->default_value("pythia.h5"), "The output file to produce")
        ("overwrite,w", po::bool_switch(&overwrite), "Allow overwriting the output file")
        ("help", "print the help message");
    
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, allOptions), vm);
    if (vm.count("help"))
    {
        std::cout << allOptions;
        return 1;
    }
    po::notify(vm);

    // Now prepare the output file
    H5::H5File fOut(outputFile, overwrite ? H5F_ACC_TRUNC : H5F_ACC_EXCL);
    auto particlePt = addHistogram(fOut, "particlePt", FixedBinAxis("$p_T$ [GeV]", 200, 0, 100));
    auto neutronPt = addHistogram(fOut, "neutronPt", FixedBinAxis("$p_T$ [GeV]", 200, 0, 100));
    auto k0LPt = addHistogram(fOut, "k0LPt", FixedBinAxis("$p_T$ [GeV]", 200, 0, 100));

    auto sigma = H5Composites::makeHandle(fOut, "xsec", 0.0);
    auto weightSum = H5Composites::makeHandle(fOut, "weightSum", 0.0);
    H5Composites::makeHandle(fOut, "pTHatMin", pTHatMin);

    Pythia8::Pythia pythia;
    pythia.settings.readString("HardQCD:all = on");
    pythia.settings.readString("PhaseSpace:pTHatMin = " + std::to_string(pTHatMin));
    pythia.settings.readString("Next:numberShowEvent = 0");
    pythia.settings.readString("Random:setSeed = on");
    pythia.settings.readString("Random:seed = " + std::to_string(seed));
    pythia.init();

    for (std::size_t iEvent = 0; iEvent < nEvents; ++iEvent)
    {
        if (!pythia.next())
            continue;
        for (std::size_t iParticle = 0; iParticle < pythia.event.size(); ++iParticle)
        {
            const Pythia8::Particle& particle = pythia.event[iParticle];
            // skip non final state particles
            if (!particle.isFinal())
                continue;
            particlePt->fill(particle.pT());
            switch (std::abs(particle.id()))
            {
                case PythANUBIS::neutronId:
                    neutronPt->fill(particle.pT());
                    break;
                case PythANUBIS::k0LId:
                    k0LPt->fill(particle.pT());
                    break;
            }
        }
    }

    *sigma = pythia.info.sigmaGen();
    *weightSum = pythia.info.weightSum();

    std::cout << "pTHatMin = " << pTHatMin << ", xsec = " << *sigma << "mb" << std::endl;
}