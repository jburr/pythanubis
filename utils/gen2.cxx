#include "PythANUBIS/defs.h"
#include "PythANUBIS/Histogram.h"
#include "PythANUBIS/FixedBinAxis.h"
#include "PythANUBIS/CategoryAxis.h"
#include "PythANUBIS/CandidateHelper.h"
#include "H5Composites/SmartBuffer.h"
#include "H5Cpp.h"
#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/PowhegHooks.h"
#include "H5Composites/TypedWriter.h"
#include "H5Composites/Handle.h"
#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"
#include <cmath>
#include <boost/program_options.hpp>
#include <vector>

static constexpr float pi = 3.141592653589793238462643383279502884;
static constexpr float minTheta = 0.828849;
static constexpr float maxTheta = 1.51461;
inline static const float minEta = -std::log(std::tan(maxTheta / 2));
inline static const float maxEta = -std::log(std::tan(minTheta / 2));
static constexpr float minKLPt = 3;
static constexpr float minNeutronPt = 5.5;
static constexpr float minChargedPt = 0.5;
static constexpr float minJetPt = 10;
static constexpr float maxChargedDR = 0.5;
static constexpr float maxChargedDR2 = maxChargedDR * maxChargedDR;
static constexpr int k0LId = 130;
static constexpr int neutronId = 2112;

namespace
{
    float dPhi(float phi1, float phi2)
    {
        return std::fmod(pi + phi1 - phi2, 2 * pi) - pi;
    }
    float dR(float eta1, float eta2, float phi1, float phi2)
    {
        float deltaEta = eta1 - eta2;
        float deltaPhi = dPhi(phi1, phi2);
        return std::sqrt(deltaEta * deltaEta + deltaPhi * deltaPhi);
    }
    bool withinDR(float eta1, float eta2, float phi1, float phi2, float dr, float dr2)
    {
        float deta = eta1 - eta2;
        if (deta > dr)
            return false;
        float dphi = dPhi(phi1, phi2);
        if (dphi > dr)
            return false;
        return (deta * deta + dphi * dphi) < dr2;
    }

}

struct GenerationDetails
{
    PythANUBIS::eventNum_t startEvent;
    PythANUBIS::eventNum_t endEvent;
    std::size_t seed;
    double sigmaGen;
    double sigmaErr;
    double weightSum;
    H5COMPOSITES_INLINE_STRUCT_DTYPE(GenerationDetails, startEvent, endEvent, seed, sigmaGen, sigmaErr, weightSum)
};

int main(int argc, const char *argv[])
{
    namespace po = boost::program_options;
    std::string settingsFile;
    std::string lheFile;
    PythANUBIS::eventNum_t nEvents;
    PythANUBIS::eventNum_t startEvent;
    std::size_t seed;
    std::string outputFile;
    std::vector<std::string> extra;
    bool overwrite = false;
    bool signal = false;
    bool writeAllCandidates = false;
    float metCut;
    po::options_description allOptions("Allowed options");
    allOptions.add_options()
        ("settings,s", po::value(&settingsFile)->required(), "The Pythia8 settings file to use")
        ("lhe,L", po::value(&lheFile), "An input LHE file to read events from")
        ("nEvents,n", po::value(&nEvents)->default_value(100), "The number of events to produce")
        ("startEvent", po::value(&startEvent)->default_value(0), "The first event number to generate")("randomSeed,r", po::value(&seed)->default_value(1), "The random seed to use")
        ("output,o", po::value(&outputFile)->default_value("pythia.h5"), "The output file to produce")("overwrite,w", po::bool_switch(&overwrite), "Allow overwriting the output file")
        ("signal,S", po::bool_switch(&signal), "This is a signal process - don't output k0 or neutrons")
        ("writeAll,W", po::bool_switch(&writeAllCandidates), "Write all candidate particles without pt or eta cuts")
        ("extra,E", po::value(&extra)->multitoken(), "Extra pythia settings")
        ("metCut,m", po::value(&metCut)->default_value(10), "The output MET cut")
        ("help", "print the help message");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, allOptions), vm);
    if (vm.count("help"))
    {
        std::cout << allOptions;
        return 1;
    }
    po::notify(vm);

    // Prepare the output
    H5::H5File fOut(outputFile, overwrite ? H5F_ACC_TRUNC : H5F_ACC_EXCL);
    H5::Group histograms = fOut.createGroup("Histograms");
    // Map from PDGID to helper
    std::map<int, CandidateHelper> helpers;
    if (signal)
        helpers.emplace(std::make_pair(
            PythANUBIS::LLPId,
            CandidateHelper(fOut, histograms, "LLPs", metCut, writeAllCandidates)));
    else
    {
        helpers.emplace(std::make_pair(
            PythANUBIS::neutronId,
            CandidateHelper(fOut, histograms, "neutrons", metCut, writeAllCandidates)));
        helpers.emplace(std::make_pair(
            PythANUBIS::k0LId,
            CandidateHelper(fOut, histograms, "K0Ls", metCut, writeAllCandidates)));
    }
    H5Composites::TypedWriter<Event> eventWriter(fOut, "events");
    H5Composites::TypedWriter<GenerationDetails> detailsWriter(fOut, "details");
    auto particlePt = H5Composites::makeHandle(
        histograms, "particlePt",
        makeHistogram<float>(FixedBinAxis("$p_T$ [GeV]", 1000, 0, 100)));
    auto nCandidates = H5Composites::makeHandle(
        histograms, "nCandidates",
        makeHistogram<float>(FixedBinAxis("# candidates", 10, 0, 10)));
    auto cutflow = H5Composites::makeHandle(
        histograms, "cutflow",
        makeHistogram<float>(CategoryAxis(" ", {"nGeneratedEvents", "hasCandidateParticle"})));
    auto met = H5Composites::makeHandle(
        histograms, "met",
        makeHistogram<float>(FixedBinAxis("$E_T^{\\mathrm{miss}}$ [GeV]", 100, 0, 100)));

    // Prepare the event generator
    Pythia8::Pythia pythia;
    // Prepare the potential user hook
    std::shared_ptr<Pythia8::UserHooks> userHook;
    pythia.readFile(settingsFile);
    pythia.settings.readString("Random:seed = " + std::to_string(seed));
    if (!lheFile.empty())
    {
        // Also force some settings
        // Frame type 4 means read beam information from an LHE file
        pythia.settings.readString("Beams:frameType = 4");
        // Set the input LHE file here
        pythia.settings.readString("Beams:LHEF = " + lheFile);
    }
    // Figure out if we're using POWHEG matching
    int vetoMode = pythia.settings.mode("POWHEG:veto");
    int mpiVetoMode = pythia.settings.mode("POWHEG:MPIveto");
    if (vetoMode > 0 || mpiVetoMode > 0)
    {
        if (vetoMode > 0)
        {
            // Set the ISR and FSR to start at the kinematic limit
            pythia.settings.readString("SpaceShower:pTmaxMatch = 2");
            pythia.settings.readString("TimeShower:pTmaxMatch = 2");
        }
        if (mpiVetoMode > 0)
            // Set MPI to start at the kinematic limit
            pythia.settings.readString("MultipartonInteractions:pTmaxMatch = 2");
        userHook = std::make_shared<Pythia8::PowhegHooks>();
    }
    for (const std::string &e : extra)
        pythia.settings.readString(e);
    if (userHook)
    {
#if PYTHIA_VERSION_INTEGER > 8300
        pythia.setUserHooksPtr(userHook);
#else
        pythia.setUserHooksPtr(userHook.get());
#endif
    }
    pythia.init();

    PythANUBIS::eventNum_t badEvents = 0;
    PythANUBIS::eventNum_t goodEvents = 0;
    std::cout << "Start generating " << nEvents << " events" << std::endl;

    std::size_t maxEvents = startEvent + nEvents;
    std::size_t iEvent = startEvent;
    for (startEvent; iEvent < maxEvents; ++iEvent)
    {
        if (!pythia.next())
        {
            if (!lheFile.empty() && pythia.info.atEndOfFile())
                // Run out of input events!
                break;
            ++badEvents;
            continue;
        }
        ++nEvents;
        cutflow->fill("nGeneratedEvents");
        // We build up the list of all particles that could be used to make jets
        std::vector<fastjet::PseudoJet> allJetInputs;
        // Build a list of the particle indices of candidates as well as their indices in the jet
        // inputs vector
        std::map<int, std::vector<std::pair<std::size_t, std::size_t>>> lists;
        for (const auto &p : helpers)
            lists[p.first] = {};
        // Also build up a list of the charged particles that we'll use in the dR test later
        std::vector<Pythia8::Particle> chargedParticles;

        Event event{
            iEvent,
            static_cast<std::size_t>(pythia.event.size()),
            0,
            0,
            pythia.info.weight(0)};
        std::size_t totalNCandidates = 0;
        for (std::size_t iParticle = 0; iParticle < pythia.event.size(); ++iParticle)
        {
            const Pythia8::Particle &particle = pythia.event[iParticle];
            // skip non final state particles
            if (!particle.isFinal())
                continue;
            particlePt->fill(particle.pT());
            for (auto &listPair : lists)
            {
                if (std::abs(particle.id()) == listPair.first)
                {
                    // Second index is the index in the jet inputs vector. If the particle is not
                    // visible then there won't be a corresponding jet input to remove so we pass
                    // SIZE_MAX which the helper knows how to deal with
                    listPair.second.emplace_back(
                        iParticle,
                        particle.isVisible() ? allJetInputs.size() : SIZE_MAX);
                    ++totalNCandidates;
                }
            }
            if (particle.isVisible())
            {
                allJetInputs.emplace_back(particle.px(), particle.py(), particle.pz(), particle.e());
                if (particle.isCharged() && particle.pT() > minChargedPt)
                    chargedParticles.push_back(particle);
            }
            else
            {
                event.mpx += particle.px();
                event.mpy += particle.py();
            }
        }
        met->fill(std::sqrt(event.mpx * event.mpx + event.mpy * event.mpy));

        if (totalNCandidates > 0)
        {
            ++goodEvents;
            cutflow->fill("hasCandidateParticle");
        }
        else
            continue;
        nCandidates->fill(totalNCandidates);
        bool writeEvent = false;
        for (const auto &p1 : lists)
            for (const auto &p2 : p1.second)
                writeEvent |= helpers.at(p1.first).processCandidate(
                    event,
                    pythia.event[p2.first],
                    chargedParticles,
                    allJetInputs,
                    p2.second);
        if (writeEvent)
            eventWriter.write(event);
    }
    GenerationDetails details;
    details.startEvent = startEvent;
    details.endEvent = iEvent;
    details.seed = seed;
    details.sigmaGen = pythia.info.sigmaGen();
    details.sigmaErr = pythia.info.sigmaErr();
    details.weightSum = pythia.info.weightSum();
    detailsWriter.write(details);
}