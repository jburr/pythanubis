#define BOOST_TEST_MODULE histo

#include <boost/test/included/unit_test.hpp>
#include "PythANUBIS/Histogram.h"
#include "PythANUBIS/CategoryAxis.h"
#include "PythANUBIS/FixedBinAxis.h"
#include "PythANUBIS/VariableBinAxis.h"
#include "H5Composites/RWTraits.h"
#include "H5Composites/SmartBuffer.h"

namespace {
    template <typename T, std::size_t N>
    std::ostream& operator<<(std::ostream& os, const std::array<T, N>& arr)
    {
        os << "[";
        if (N > 0)
        {
            auto itr = arr.begin();
            for (; itr != arr.end() - 1; ++itr)
                os << *itr << ", ";
            os << *itr;
        }
        return os << "]";
    }
}


BOOST_AUTO_TEST_CASE(fill)
{
    using namespace H5Composites;
    std::cout << "Create h1" << std::endl;
    auto h1 = makeHistogram<float>(CategoryAxis("Type", {"A", "B", "C", "D", "E"}));
    std::cout << "Fill h1" << std::endl;
    h1.fill("A");
    h1.fill("C");
    h1.fill("B", 5);
    h1.fill("X");
    BOOST_TEST(h1.contents("A") == 1);
    BOOST_TEST(h1.contents("C") == 1);
    BOOST_TEST(h1.contents("B") == 5);
    BOOST_TEST(h1.contents("overflow") == 1);

    std::cout << "Create h2" << std::endl;
    auto h2 = makeHistogram<float>(
        FixedBinAxis("x", 10, -5, 5),
        FixedBinAxis("y", 20, 10, 50)
    );
    std::cout << "Fill h2" << std::endl;
    auto idx = h2.findBin(-3.5, 49);
    std::cout << std::get<0>(idx) << ", " << std::get<1>(idx) << std::endl;
    h2.fill(-3.5, 49);
    h2.fill(3.5, 11, 2);
    h2.fill(0, 30);
    h2.fill(3.1, 10.5, 3);
    BOOST_TEST(h2.contents(2, 20) == 1);
    BOOST_TEST(h2.contents(9, 1) == 5);
    BOOST_TEST(h2.sumW2(9, 1) == 13);
    BOOST_TEST(h2.contents(6, 11) == 1);

    SmartBuffer buffer(h2.h5DType().getSize());
    std::cout << "Write to buffer" << std::endl;
    h2.writeBuffer(buffer.get());
    std::cout << "Read from buffer" << std::endl;
    auto h3  = buffer_read_traits<Histogram<double, FixedBinAxis, FixedBinAxis>>::read(buffer.get(), h2.h5DType());
    std::cout << "Start check" << std::endl;
    BOOST_TEST(h2.contents(3, 20) == h3.contents(3, 20));
    BOOST_TEST(h2.sumW2(3, 20) == h3.sumW2(3, 20));
    BOOST_TEST(h2.contents(9, 1) == h3.contents(9, 1));
    BOOST_TEST(h2.sumW2(9, 1) == h3.sumW2(9, 1));
    BOOST_TEST(h2.contents(1, 11) == h3.contents(1, 11));
    BOOST_TEST(h2.sumW2(1, 11) == h3.sumW2(1, 11));
}