#define BOOST_TEST_MODULE axis

#include <boost/test/included/unit_test.hpp>
#include "PythANUBIS/CategoryAxis.h"
#include "PythANUBIS/FixedBinAxis.h"
#include "PythANUBIS/VariableBinAxis.h"
//#include "PythANUBIS/GenericAxis.h"
#include "H5Composites/SmartBuffer.h"
#include "H5Composites/RWTraits.h"
#include <stdexcept>

namespace {
    template <typename T>
    std::ostream& operator<<(std::ostream& os, const std::vector<T>& v)
    {
        os << "[";
        if (v.size() > 0)
        {
            auto itr = v.begin();
            for (; itr != v.end() - 1; ++itr)
                os << *itr << ", ";
            os << *itr;
        }
        return os << "]";
    }
#if 0
    std::ostream& operator<<(std::ostream& os, GenericAxis::AxisType axisType)
    {
        switch(axisType)
        {
            case GenericAxis::AxisType::CATEGORY:
                return os << "CATEGORY";
            case GenericAxis::AxisType::FIXED_BIN:
                return os << "FIXED_BIN";
            case GenericAxis::AxisType::VARIABLE_BIN:
                return os << "VARIABLE_BIN";
            default:
                return os << "INVALID";
        }
    }
#endif
}

//BOOST_TEST_DONT_PRINT_LOG_VALUE(GenericAxis::AxisType)

BOOST_AUTO_TEST_CASE(category)
{
    using namespace H5Composites;
    CategoryAxis axis("Test", {"A", "B", "C", "D", "E"});
    SmartBuffer buffer(axis.h5DType().getSize());
    axis.writeBuffer(buffer.get());
    CategoryAxis axis2 = buffer_read_traits<CategoryAxis>::read(buffer.get(), axis.h5DType());
    BOOST_TEST(axis.name() == axis2.name());
    BOOST_TEST(axis.categories() == axis2.categories());
    BOOST_TEST(axis.binOffset("C") == 2);
    BOOST_TEST(axis.binOffset("X") == 5);
}

BOOST_AUTO_TEST_CASE(fixed)
{
    using namespace H5Composites;
    FixedBinAxis axis("Test", 10, -5, 5);
    SmartBuffer buffer(axis.h5DType().getSize());
    axis.writeBuffer(buffer.get());
    FixedBinAxis axis2 = buffer_read_traits<FixedBinAxis>::read(buffer.get(), axis.h5DType());
    BOOST_TEST(axis.name() == axis2.name());
    BOOST_TEST(axis.min() == axis2.min());
    BOOST_TEST(axis.max() == axis2.max());
    BOOST_TEST(axis.nBins() == axis2.nBins());

    BOOST_TEST(axis.findBin(-0.5) == 5);
    BOOST_TEST(axis.findBin(-5) == 1);
    BOOST_TEST(axis.findBin(-5.1) == 0);
    BOOST_TEST(axis.findBin(100) == 11);
}

BOOST_AUTO_TEST_CASE(variable)
{
    using namespace H5Composites;
    VariableBinAxis axis("Test", {-5, -4, -3, -2, -1, -0.5, 0, 0.5, 10});
    SmartBuffer buffer(axis.h5DType().getSize());
    axis.writeBuffer(buffer.get());
    VariableBinAxis axis2 = buffer_read_traits<VariableBinAxis>::read(buffer.get(), axis.h5DType());
    BOOST_TEST(axis.edges() == axis2.edges());

    BOOST_TEST(axis.findBin(0.3) == 7);
    BOOST_TEST(axis.findBin(-100) == 0);
    BOOST_TEST(axis.findBin(10) == 9);
}

#if 0
BOOST_AUTO_TEST_CASE(generic)
{
    using namespace H5Composites;
    VariableBinAxis axis("Test", {-5, -4, -3, -2, -1, -0.5, 0, 0.5, 10});
    SmartBuffer buffer(axis.h5DType().getSize());
    axis.writeBuffer(buffer.get());
    GenericAxis axis2 = buffer_read_traits<GenericAxis>::read(buffer.get(), axis.h5DType());
    
    BOOST_TEST(axis2.axisType() == GenericAxis::AxisType::VARIABLE_BIN);
    BOOST_TEST(axis2.name() == "Test");
    BOOST_TEST(axis2.get<VariableBinAxis>().edges() == axis.edges());
    BOOST_TEST(axis2.nBins() == 8);
    BOOST_TEST(axis2.findBin(0.3) == 8);
    BOOST_CHECK_THROW(axis2.findBin("X"), std::invalid_argument);

    GenericAxis axis3 = CategoryAxis("Test", {"A", "B", "C", "D", "E"});
    BOOST_TEST(axis3.binOffset("B") == 2);
    BOOST_TEST(axis3.nBins() == 5);
    BOOST_CHECK_THROW(axis3.findBin(0.3), std::invalid_argument);

}
#endif